/*
 * Copyright (C) 1981, 1987 Masataka Ohta, Hiroshi Tezuka
 * Copyright (C) 2016-2021 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  main.c --- main entry
 *
 */

int main(int argc, char **argv)
{	char *av0;
	int i, arg;
	av0 = argv[0];
	earlyinit();
	for ( i = 1; i < argc; i++ ) {
		if ( *argv[i] == '-' && argv[i][1] != '\0' ) {
			arg = 0;
			as_free(fname);
			fname = as_strdup(argv[i]); /* <- for error() */
			if ( argv[i][2] != '\0' ) {
				arg = 1;
				switch ( argv[i][1] ) {
				case 's':
					symbol = atoi(&argv[i][2]);
					break;
				case 'O':
					optimize = atoi(&argv[i][2]);
					break;
				default:
					++argv[i];
					goto illopt;
				}
			}
			switch ( *++argv[i] ) {
#ifdef	DEBUGOPT
			case 'd':
				debug = 1;
				break;
#endif
			case 'l':
				list = 1;
				if ( !argv[i+1] || (*argv[i+1] == '-' && argv[i+1][1] != '\0') ) continue;
				if ( *argv[++i] )
					lstfile = argv[i];
				break;
			case 'o':
				object = 1;
				if ( !argv[i+1] || (*argv[i+1] == '-' && argv[i+1][1] != '\0') ) continue;
				if ( *argv[++i] )
					objfile = argv[i];
				break;
			case 's':
				symbol = symbol >= 0 ? symbol : 0;
				if ( !argv[i+1] || (*argv[i+1] == '-' && argv[i+1][1] != '\0') ) continue;
				if ( *argv[++i] )
					symfile = argv[i];
				break;
			case 'e':
				fatalerror = 1;
				break;
#if	RAW_FORMAT || IHEX_FORMAT || DECB_FORMAT
			case 'f':
				argcheck(argv[++i]);
#if	RAW_FORMAT
				if (!strcmp(argv[i],"r")) /* Raw binary */
					format = RAW_FORMAT;
				else
#endif
#if	IHEX_FORMAT
				if (!strcmp(argv[i],"i")) /* Intel HEX */
					format = IHEX_FORMAT;
				else
#endif
#if	DECB_FORMAT
				if (!strcmp(argv[i],"d")) /* DECB binary */
					format = DECB_FORMAT;
				else
#endif
				if (!strcmp(argv[i],"m")) /* Motorola SREC (default) */
					format = SREC_FORMAT;
				else
					fatal("wrong format \"%s\"",argv[i],OPTERROR,0);
				break;
#endif
			case 'p':
				onepass = 1;
				break;
			case 'q':
				quiet = 1;
				break;
			case 'u':
				undef = 1;
				break;
			case 'v':
				verbos++;
				break;
			case 'x':
				defextend = 1;
				break;
#ifdef	MC6809
			case 'B':
				def_optlongbranch = 1;
				break;
#endif
			case 'D':
				argcheck(argv[++i]);
				defsymbol(argv[i]);
				break;
			case 'I':
				argcheck(argv[++i]);
				addincdir(argv[i]);
				break;
#ifdef	SYSINCDIR
			case 'N':
				nosysinc = 1;
				break;
#endif
			case 'O':
				if (!arg)
					optimize = 1;
				break;
			case 'P':
				argcheck(argv[++i]);
				parseoption(argv[i],1);
				break;
#ifndef	MC6809
			case 'X':
				mc6801 = 1;
				break;
#endif
			case 0:
				break;
			default:
illopt:				printtitle(0);
				fatal("illegal option -%s",argv[i],OPTERROR,0);
			}
		}
		else {
			if (srcfile)
				fatal("extra source file",NULL,ARGERROR,0);
			srcfile = argv[i];
		}
	}
	if ( !srcfile ) {
		if ( argc < 1 )
			fputs("weird command line\n",stderr);
		else
		{	printtitle(1);
			if ( argc == 1 )
				printusage(av0);
			else
				fputs("no input file\n",stderr);
		}
		IF_CLEANUP(cleanup();)
		return argc == 1 ? 0 : ARGERROR;
	}
	printtitle(0);
	if ( !strcmp(srcfile,"-") )
		srcf = stdin;
	if ( object && !objfile ) {
		objfile = as_strconcat2(srcfile,".o");
		IF_CLEANUP(objarg = 1;)
	}
	if ( list && !lstfile ) {
		lstfile = as_strconcat2(srcfile,".l");
		IF_CLEANUP(lstarg = 1;)
	}
	if ( symbol >= 0 && !symfile ) {
		symfile = as_strconcat2(srcfile,".s");
		IF_CLEANUP(symarg = 1;)
	}
	initialize();
	if ( !onepass ) {
		pass = 1;
		do {
			if ( totpass >= MAXPASS ) {
				fputs("maximum number of passes reached\n",stderr);
				break;
			}
			assemble();
		} while ( optimize && nchange );
	}
	pass = 0;
	if ( list ) {
		if ( !strcmp(lstfile, "-") )
			lstf = stdout;
		else
			lstf = fileopen(lstfile,"w");
	}
	if ( symbol >= 0 ) {
		if ( !strcmp(symfile, "-") )
			symf = stdout;
		else
			symf = fileopen(symfile,"w");
	}
	if ( object ) {
		if ( !strcmp(objfile, "-") )
			objf = stdout;
		else
#if	defined(_WIN32) && (RAW_FORMAT || DECB_FORMAT)
			if (
#if	RAW_FORMAT
			    format == RAW_FORMAT ||
#endif
#if	DECB_FORMAT
			    format == DECB_FORMAT ||
#endif
			    0
			   )
				objf = fileopen(objfile,"wb");
			else
#endif
			objf = fileopen(objfile,"w");
	}
	assemble();
	if ( object )
		fileclose(objf,objfile);
	if ( symbol >= 0 )
		fileclose(symf,symfile);
	if ( list )
		fileclose(lstf,lstfile);
	IF_CLEANUP(cleanup();)
	return errors ? GENERROR : 0;
}

static
void earlyinit()
{
	symbol = -1;
	initoptions();
	initlabel();
}

static
void argcheck(const char *arg)
{
	if ( !arg )
		fatal("missing argument to %s option",fname,OPTERROR,0);
}

static
void initialize()
{	OPTABLE *p, *q;
	int h;
	if ( srcf == stdin )
		onepass = 1;
	libcount = 0;
	as_free(fname);
	fname = as_strdup(srcfile);
	for ( p = optab; *p->mnemonic; p++ ) {
		q = ophash[ h = hash( p->mnemonic ) ];
		if ( q ) {
			while ( q->nl != NULL ) q = q->nl;
			q->nl = p;
		}
		else {
			ophash[ h ] = p;
		}
		p->nl = NULL;
	}
#ifdef SYSINCDIR
	if ( !nosysinc )
		addincdir(SYSINCDIR);
#endif
}

static
char *lineget()
{	char *i, *line;
	int a, size;
	lineptr = linebuf + LINEHEAD;
	size = MAXCHAR - LINEHEAD - 1;
#if	WITH_STRUCT
	if ( struct_level && struct_getline(lineptr,size) ) {
		expand = structstk->structure->expand;
		slineno++;
		return lineptr;
	}
#endif
	if ( macrostk ) {
		line = macro_getline(lineptr,size);
		if ( line ) {
			expand = macrostk->macro->expand;
			slineno++;
			return line;
		}
	}
	if ( linetoread == 0 )
		return NULL;
	while ( (i = fgets(lineptr,size,srcf)) != NULL ) {
		if ( linetoskip ) {
			linetoskip--;
			continue;
		}
		a = strlen(i);
		if ( a >= MAXCHAR-LINEHEAD-1-1 )
			fatal("line too long",NULL,LTLERROR,2);
		if ( a>=2 && i[a-2] == '\r' && i[a-1] == '\n' )
			{ i[a-2] = EOL; i[a-1] = '\0'; }
		else if ( a>=1 && i[a-1] != EOL )
			{ i[a] = EOL; i[a+1] = '\0'; }
		else if ( a==0 )
			{ i[0] = EOL; i[1] = '\0'; }
		linetoread--;
		break;
	}
	if ( ferror(srcf) )
		fatal("can't read file",NULL,FIOERROR,2);
	expand = 1;
	slineno = 0;
	return i;
}

static
void assemble()
{	initpass();
	while ( !endfile ) {
		if ( lineget() == NULL )
			popfile();
		else {
			initline();
			operation();
DEBUG(linebuf[lndigit+4] = ' '+!COND_IS_TRUE(cond);)
			putline();
		}
	}
	terminate();
}

static
void initlabel()
{
	as_free(parentlabel);
	parentlabel = as_strdup(""); /* default non-local label */
}

static
void initpass()
{	blankline = flineno = slineno = dlc = lc = endfile = nodp = objlc = objbias = objpos = sfp = nchange = 0;
#ifdef	MC6809
	optbrcnt = dp = 0;
#endif
#if	WITH_OS9
	os9_crc = 0;
#endif
	linetoskip = 0;
	linetoread = -1; /* no limit */
	dlcvalid = lcvalid = 1;
	totsize = 0;
	totpass++;
	initoptions();
	calclndigit();
	llineno = 0;
	cond = COND_NONE;
	initlabel();
	macro_init();
#if	WITH_STRUCT
	struct_init();
#endif
	src_resetinst(cursrc);
	if ( verbos > 1 ) {
		fprintf(stderr,"<pass %d>\n",totpass);
		fprintf(stderr,"SRC: %s\n",srcfile);
	}
	if ( srcf != stdin ) {
		srcf = fileopen(srcfile,"r");
		src_set(srcfile);
	}
}

static
void initline()
{	char *p;
	lblptr = (LBLTABLE *)0;
	postf = 0;
	pos = LOO;
	dataop = 0;
	sizeinvalid = 0;
	noundef = 0;
	for ( p = linebuf+lndigit; p < linebuf + LINEHEAD; p++) *p = ' ';
	if ( !macrostk
#if	WITH_STRUCT
	     && !struct_level
#endif
	   )
	{	++flineno;
		++srclineno;
		err = 0;
	}
	if ( expand )
		++llineno;
	printdecimal(llineno,linebuf);
	printaddress(llc = lc);
	dllc = dlc;
	if ( reflbl )
	{	as_free(reflbl);
		reflbl = NULL;
	}
}

static
void operation()
{	char temp[MNEMOBUFSIZE + 1], macro_found;
	char blank; /* 1=blank 2=comment */
	char *p, *pp, *tlptr, *lineptr2, *tlptrend;
	unsigned char prefix;
	unsigned int prev_macro_defs;
	MACROTABLE *macro;
#if	WITH_STRUCT
	STRUCTTABLE *strct;
#endif
#if	DOTDIR
	int dotdir;
#endif
	OPTABLE *q, *mnemo;
	macro = NULL;
	mnemo = NULL;
#if	WITH_STRUCT
	strct = NULL;
#endif
	prefix = 0;
	blank = 0;
	prev_macro_defs = macro_defs;
	linebeg = lineptr;
	if ( iscomment(*lineptr) )
	{	if ( *lineptr == ';' )
			srcnameset();
		blank = 1 + *lineptr != EOL;
	}
	else
	{	tlptr = lineptr;
		skipnonseparator();
		if ( isspace(*lineptr) )
		{	skipspace();
			if ( !iscomment(*lineptr) )
			{
#if	DOTDIR
				dotdir = checkchar('.');
#endif
#if	WITH_STRUCT
				lineptr2 = lineptr;
#endif
				for ( p = temp, pp = temp + MNEMOBUFSIZE; p < pp; p++,lineptr++ )
					if ( isseparator(*p = (char)toupper(*lineptr)) ) break;
				if (p == pp && !isseparator(*lineptr) )
					error("mnemonic too long");
				*p = '\0';
				while ( !isseparator(*lineptr) ) lineptr++;
				macro_found = !prev_macro_defs && (macro=macro_check(temp));
				if ( !macro_found )
				{	for ( q = ophash[ hash( temp ) ]; q != NULL; q = q->nl )
					{	if ( strcmp(temp,q->mnemonic) == 0 )
						{
							prefix = q->prefix;
#if	DOTDIR == 1
							if ( dotdir && (prefix < 0x80) ) break;
#elif	DOTDIR == 2
							if ( dotdir == (prefix < 0x80) ) break;
#endif
#ifndef	MC6809
							if ( !mc6801 && q->option == MC6801 ) break;
#endif
							mnemo = q;
							break;
						}
					}
					if ( !prev_macro_defs && mnemo == NULL ) {
#if	WITH_STRUCT
						for ( p = temp, pp = temp + MNEMOBUFSIZE; p < pp; p++,lineptr2++ )
							if ( isseparator(*p = *lineptr2) ) break;
						*p = '\0';
						strct = struct_check(temp);
						if ( strct )
							prefix = STRC;
						else
#endif
#if	DOTDIR
						error(dotdir ? "unrecognizable directive" : "unrecognizable mnemonic");
#else
						error("unrecognizable mnemonic");
#endif
					}
				}
			}
			else
				blank = 1 + *lineptr != EOL;
		}
		else
			blank = 1;

		if ( !prev_macro_defs ) {
#if	WITH_STRUCT
			if ( !struct_defs ) {
#endif
			tlptrend = lineptr;
			lineptr = tlptr;
			if ( !isspace(*lineptr) )
			{	if ( prefix == COND )
					error("label not allowed on %s", "IF/ELIF/ELSE/ENDC");
				else
					if ( COND_IS_TRUE(cond) && (prefix & NOLABEL_MASK) != NOLABEL_MASK )
						deflabel();
			}
			skipspace();
			if ( macro != NULL )
			{	lineptr = tlptrend;
				if ( COND_IS_TRUE(cond) )
					macro_enter(macro);
			}
			else if ( mnemo != NULL )
			{	lineptr = tlptrend;
				if ( COND_IS_TRUE(cond) || prefix == COND )
				{
					(*(oprptr=mnemo)->process)();
#if	WITH_STRUCT
					if ( !struct_from_def ) {
#endif
					if ( dataop )
					{	if ( sizeinvalid == 2 ) /* new valid ORG */
							dlcvalid = 1;
						else
						{	if ( sizeinvalid == 1 )
								dlcvalid = 0;
							if ( !inp1 )
								totsize += dlc-dllc;
						}
					}
					else
					{	if ( sizeinvalid == 2 ) /* new valid ORG */
							lcvalid = 1;
						else
						{	if ( sizeinvalid == 1 )
								lcvalid = 0;
							if ( !inp1 )
								totsize += lc-llc;
						}
					}
#if	WITH_STRUCT
					}
#endif
				}
			}
#if	WITH_STRUCT
			else if ( strct != NULL )
			{	lineptr = tlptrend;
				if ( COND_IS_TRUE(cond) )
					struct_enter(strct, 0);
			}
			}
			else /* struct_defs */ {
				/* allow macro expansion inside struct definition */
				if ( macro != NULL ) {
					if (!isspace(*linebeg))
						error("label not allowed");
					macro_enter(macro);
				}
				else {
					if ( !(blank == 2 || (blank == 1 && isseparator(*linebeg))) && prefix != STRC && prefix != STRD && prefix != ENDS && prefix != RMB && prefix != COND && prefix != OKSD )
						error("%s not allowed inside STRUCT", mnemo ? mnemo->mnemonic : "label");
					else if ( prefix == ENDS || prefix == STRD )
						(*(oprptr=mnemo)->process)();
				}
			}
#endif
			if ( setlabel )
			{	setlabel = 0;
				error("label was previously SET");
			}
			else if ( mnemo != NULL && *lineptr != EOL && !isspace(*lineptr) )
				error("extra garbage");
		}
		else if ( prefix == MACR )
			(*(oprptr=mnemo)->process)();
	}
	if ( prev_macro_defs && macro_defs && !macro_skip )
		macro_addline(linebeg);
#if	WITH_STRUCT
	if ( struct_defs )
		struct_addline(linebeg, prefix);
#endif
	if ( prev_macro_defs || macro_defs || !COND_IS_TRUE(cond) || (lblptr == NULL && (mnemo == NULL || prefix > DATA))
#if	WITH_STRUCT
	     || struct_defs
#endif
	   )
		clearaddress();
	if ( blank == 1 && !macrostk && isseparator(*linebeg) )
		blankline++;
}

static
void skipspace()
{	while ( isspace(*lineptr) ) lineptr++;
}

static
void skipnonseparator()
{	while ( !isseparator(*lineptr) ) lineptr++;
}

static
int checkgarbage(void)
{
	if ( *lineptr != EOL && !isspace(*lineptr) ) {
		error("extra garbage");
		return 1;
	}
	return 0;
}

/* set source name and line number */
/* with comment in the form of:    */
/* '; linenum "filename" 1|2'      */
static
void srcnameset()
{	char *namebeg, *nameend;
	if ( lineptr[1] == ' ' && isdigit(lineptr[2]) )
	{	namebeg = strchr(lineptr+3, ' ');
		if ( namebeg && namebeg[1] == '\"' )
		{	nameend = strchr(namebeg+2, '\"');
			if ( nameend && nameend[1] == ' ' && (nameend[2] == '1' || nameend[2] == '2') && nameend[3] == EOL )
			{	as_free(srcname);
				srcname = NULL;
				if ( nameend[2] == '1' )
				{	srclineno = atoi(lineptr+2)-1;
					if ( srclineno >= 0 )
						srcname = as_strndup(namebeg+2,nameend-namebeg-2);
				}
			}
		}
	}
}

#ifdef	MC6809
static
void operand(int grp, int mode)
{	int val,reg;
	skipspace();
	if ( (mode & (IMMEDIATE | IMMEDIATE2)) && checkchar('#') )
	{	putcode(grp,IMMEDIATE_MODE);
		val = expression();
		if ( mode & IMMEDIATE )
		{	if ( val < -128 || 255 < val )
				valueerror();
			putbyte(val);
		}
		else putword(val);
		return;
	}
	indirect = checkchar('[');
	if ( (mode & INDEX) && checkchar(',') )
	{	putcode(grp,INDEX_MODE);
		if ( checkchar('-') )
		{	if ( checkchar('-') ) indexed(0x83,getreg(INDEXREG));
			else if ( indirect ) error("illegal indirect mode");
			else indexed(0x82,getreg(INDEXREG));
		}
		else
		{	reg = getreg(INDEXREG);
			if ( checkchar('+') )
				if ( checkchar('+') ) indexed(0x81,reg);
				else if ( indirect )
						error("illegal indirect mode");
				else indexed(0x80,reg);
			else indexed(0x84,reg);
		}
	}
	else if ( (mode & INDEX) && checkreg('A') )
		if ( checkchar(',') )
		{	putcode(grp,INDEX_MODE);
			indexed(0x86,getreg(INDEXREG));
		}
		else error("invalid accumulator offset");
	else if ( (mode & INDEX) && checkreg('B') )
		if ( checkchar(',') )
		{	putcode(grp,INDEX_MODE);
			indexed(0x85,getreg(INDEXREG));
		}
		else error("invalid accumulator offset");
	else if ( (mode & INDEX) && checkreg('D') )
		if ( checkchar(',') )
		{	putcode(grp,INDEX_MODE);
			indexed(0x8b,getreg(INDEXREG));
		}
		else error("invalid accumulator offset");
	else
	{	fivebit = 1;
		val = expression();
		if ( indirect && fivebit )
		{	error("signed 5-bit offset not allowed");
			fivebit = 0;
		}
		if ( (mode & INDEX) && checkchar(',') )
		{	putcode(grp,INDEX_MODE);
			switch ( reg = getreg(INDEXREG | PC | PCR) )
			{ case X: case Y: case U: case S:
				/* when optimization are enabled, */
				/* operand size is enforced when specified */
				if ( optimize && fivebit )
				{	checkfivebit(val);
					indexed(val & 0x1f,reg);
				}
				else if ( (!indirect && checkfivebit(val)) || (indirect && !val && valid && !byte && !word) )
				{	indexed(val ? (val & 0x1f) : 0x84,reg);
				}
				else if ( checkbyte(val) )
				{	indexed(0x88,reg);
					putbyte(val);
				}
				else
				{	indexed(0x89,reg);
					putword(val);
				}
				break;
			case PC:
				if ( optpc )
					goto pcr;
				if ( fivebit )
					error("signed 5-bit offset not allowed");
				if ( checkbyte(val) )
				{	indexed(0x8c,0);
					putbyte(val);
				}
				else
				{	indexed(0x8d,0);
					putword(val);
				}
				break;
			case PCR:
pcr:				if ( fivebit )
					error("signed 5-bit offset not allowed");
				val -= llc + 3 + !!oprptr->prefix;
				if ( checkbyte(val) )
				{	indexed(0x8c,0);
					putbyte(val);
				}
				else
				{	indexed(0x8d,0);
					putword(val - 1);
				}
			}
		}
		else if ( !fivebit && (mode & INDEX) && indirect )
		{	putcode(grp,INDEX_MODE);
			postbyte(0x9f);
			putword(val);
		}
		else if ( !fivebit && (mode & DIRECT) && (byte || ((unsigned)(val - (dp << 8)) <= 255 && valid && !word && !defextend && !nodp)) )
		{	putcode(grp,DIRECT_MODE);
			putbyte(val - (dp << 8));
		}
		else if ( !fivebit && (mode & EXTEND) )
		{	putcode(grp,EXTEND_MODE);
			putword(val);
		}
		else error("illegal addressing mode");
		fivebit = 0;
	}
	if ( indirect && !checkchar(']') ) error("missing ']'");
}

static
void indexed(int frame, int reg)
{	int xr;
	switch ( reg )
	{	case X: xr = 0x00; break;
		case Y: xr = 0x20; break;
		case U: xr = 0x40; break;
		case S: xr = 0x60; break;
		default: xr = 0;
	}
	postbyte(frame | xr | (indirect ? 0x10 : 0));
}
#else
static
void operand(int grp, int mode)
{	int val;
	skipspace();
	if ( (mode & (IMMEDIATE | IMMEDIATE2)) && checkchar('#') )
	{	putcode(grp,IMMEDIATE_MODE);
		val = expression();
		if ( mode & IMMEDIATE )
		{	if ( val < -128 || 255 < val )
				valueerror();
			putbyte(val);
		}
		else putword(val);
		return;
	}
	if ( (mode & INDEX) && checkchar(',') && checkchar('X') )
	{	putcode(grp,INDEX_MODE);
		putbyte( 0 );
		return;
	}
	val = expression();
	if ( (mode & INDEX) && checkchar(',') && checkchar('X') )
	{	putcode(grp,INDEX_MODE);
		if ( val < 0 || 255 < val )
			error( "offset out of range" );
		putbyte( val );
	}
	else if ( (mode & DIRECT) && (byte || 
		((unsigned)val <= 255 && valid && !word && !defextend && !nodp)) )
	{	putcode(grp,DIRECT_MODE);
		putbyte(val);
	}
	else if ( mode & EXTEND )
	{	putcode(grp,EXTEND_MODE);
		putword(val);
	}
	else error("illegal addressing mode");
}
#endif

static
int term()
{	int tv;
#if	WITH_WEIRDCONST
	char n;
#endif
#if	WITH_SIZEOF || WITH_DEFINED
	char *lb1, *tlineptr;
#endif
#if	WITH_SIZEOF
	char *lb2;
#endif
	LBLTABLE *lp;
	GLOBLTABLE *globl;
	switch ( *lineptr++ )
	{	case '-': return -term();
		case '+': return term();
		case '~': case '^': return ~term();
		case '!': return !term();
		case '*':
retllc:			if ( !lcvalid )
				valid = 0;
			return llc;
		case '\'':
			if ( *lineptr == EOL )
				goto unexp;
			else
			{	tv = (unsigned char)*lineptr++;
				checkchar('\'');
			}
			return tv;
		case '\"':
			if ( *lineptr == EOL )
				goto unexp;
			else
			{	tv = (unsigned char)*lineptr++;
				if ( *lineptr == EOL )
					goto unexp;
				tv = tv << 8 | (unsigned char)*lineptr++;
				checkchar('\"');
			}
			return tv;
		case '$':
#if	WITH_WEIRDCONST
			n = checkchar('-');
#endif
			if ( !isxdigit(*lineptr) )
				goto illchar;
			for ( tv = 0; isxdigit(*lineptr); lineptr++)
				tv = tv * 16 +
					(isdigit(*lineptr) ? (*lineptr - '0') :
					  (toupper(*lineptr) - 'A' + 10));
			return
#if	WITH_WEIRDCONST
				n ? -tv :
#endif
				tv;
		case '%':
#if	WITH_WEIRDCONST
			n = checkchar('-');
#endif
			if ( !(*lineptr == '0' || *lineptr == '1') )
				goto illchar;
			for ( tv = 0;
				*lineptr == '0' || *lineptr == '1'; lineptr++)
					tv = tv * 2 + *lineptr - '0';
			return
#if	WITH_WEIRDCONST
				n ? -tv :
#endif
				tv;
		case '@':
#if	WITH_ATLABEL
			if ( optnodot && isalpha(*lineptr) ) {
				lineptr--;
				goto atlabel;
			}
#endif
#if	WITH_WEIRDCONST
			n = checkchar('-');
#endif
			if ( !(*lineptr >= '0' && *lineptr <= '7') )
				goto illchar;
			for ( tv = 0;
				*lineptr >= '0' && *lineptr <= '7'; lineptr++)
					tv = tv * 8 + *lineptr - '0';
			return
#if	WITH_WEIRDCONST
				n ? -tv :
#endif
				tv;
		case '(':
			tv = expression();
			if ( !checkchar(')') ) {
				error("missing ')'");
				experr = 1; }
			return tv;
		default:
			if ( lineptr[-1] == '.' && !isalpha(lineptr[0]) )
			{
				if ( !optdatsep )
					goto retllc;
				if ( !dlcvalid )
					valid = 0;
				return dllc;
			}
			else if ( isalpha(*--lineptr) )
			{
#if	WITH_DEFINED
				if ( !as_strncmpi(lineptr, "DEFINED{", 8) ) {
					lineptr += 8;
					lb1 = getlabel();
					tlineptr = lineptr;
					lineptr = lb1;
					tv = !!reflabel(REF_DEFINED);
					lineptr = tlineptr;
					as_free(lb1);
					if ( !checkchar('}') )
						error("missing '}'");
					return tv;
				}
				else
#endif
#if	WITH_SIZEOF
				if ( !as_strncmpi(lineptr, "SIZEOF{", 7) ) {
					lineptr += 7;
					lb1 = getlabel();
					lb2 = as_strconcat3(lb1, *lb1 ? SIZEOF_TAG : "" , "");
					as_free(lb1);
					tlineptr = lineptr;
					lineptr = lb2;
					lp = reflabel(REF_EXPRESSION);
					lineptr = tlineptr;
					as_free(lb2);
					if ( !checkchar('}') )
						error("missing '}'");
				}
				else
#endif
#if	WITH_ATLABEL
atlabel:
#endif
				lp = reflabel(REF_EXPRESSION);
				if ( lp )
				{
					if ( (optimize < 2 || pass == 0) && !lp->valid )
						valid = 0;
					else if ( !optimize && (lp->pass && lp->pass < totpass && !lp->inp1) && (lp->type!=SYM_SET || !optundefset) )
						valid = 0;
					else if ( (globl=FIND_GLOBL(lp->t.name, &privglobltbl, NULL)) && (globl->flag & GLOBAL_TYPE_MASK) == GLOBAL_TYPE_XREF )
						valid = 0;
					if ( noundef && !valid )
						UNDEFINED_SYMBOL();
					return lp->value;
				}
				if ( noundef != 2 )
				{
					UNDEFINED_SYMBOL();
					valid = 0;
				}
				return 0;
			}
			else if ( isdigit(*lineptr) )
			{	for ( tv = 0; isdigit(*lineptr); lineptr++)
					tv = tv * 10 + *lineptr - '0';
				return tv;
			}
			else
			{
illchar:			error("illegal character in term");
DEBUG(fprintf(stderr,"*lineptr : %c\n",*lineptr);)
				experr = 1;
				return 0;
			}
	}
unexp:
	error("unexpected end of line");
	experr = 1;
	return 0;
}

static
void valueerror(void)
{
	if ( !optvalrange )
		error("value range error");
}

static
void error(const char *msg, ...)
{	char *filename;
	int linenum;
	va_list ap;
	if ( (pass && !inp1) || err ) return;
	err = 1;
	errors++;
	filename = srcname ? srcname : fname;
	linenum = srcname ? srclineno : flineno;
	fprintf(stderr,linenum?FILELINE:FILELINENOLN,filename,linenum);
	va_start(ap,msg);
	vfprintf(stderr,msg,ap);
	va_end(ap);
	putc('\n',stderr);
	if ( lstf != NULL )
	{	fputs("*** ",lstf);
		va_start(ap,msg);
		vfprintf(lstf,msg,ap);
		va_end(ap);
		putc('\n',lstf);
	}
	if ( fatalerror )
	{	if ( !quiet )
			putc('\n',stderr);
		IF_CLEANUP(cleanup();)
		exit(GENERROR);
	}
}

static
void fatal(const char *msg, const char *str, int code, int lineoffset)
{	char *filename;
	int linenum;
	if ( lineoffset )
	{	filename = srcname ? srcname : fname;
		linenum = (srcname ? srclineno : flineno) + lineoffset - 1;
		fprintf(stderr,FILELINE,filename,linenum);
	}
	if ( fname )
		fputs("fatal error: ",stderr);
	if ( str == NULL )
		fputs(msg,stderr);
	else
		fprintf(stderr,msg,str);
	putc('\n',stderr);
	IF_CLEANUP(cleanup();)
	if ( code != 0 )
		exit(code);
}

static
FILE *fileopen(char *filename, const char *mode)
{	FILE *f;
DEBUG(fprintf(stderr,"fileopen \"%s\", \"%s\"\n",filename,mode);)
	f = fopen(filename,mode);
	if ( f == NULL )
		fatal("can't open \"%s\"",filename,FIOERROR,0);
	return f;
}

static
void fileclose(FILE *f, char *filename)
{
DEBUG(fprintf(stderr,"fileclose \"%s\"\n",filename);)
	if ( (f == stdout && fflush(f)) || (f != stdout && fclose(f)) )
		fatal("failed to close \"%s\"\n",filename,FIOERROR,0);
}

#ifdef	MC6809
static
int regno()
{	switch ( getreg(ALLREG) )
	{	case D: return 0;
		case X: return 1;
		case Y: return 2;
		case U: return 3;
		case S: return 4;
		case PC: return 5;
		case A: return 8;
		case B: return 9;
		case CC: return 10;
		case DP: return 11;
		default:
			error("unrecognizable register");
			return 0;
	}
}
#endif

static
int pushfile(FILE *fp, char *filename, char *namspc, int lineno, int numline, int global)
{	LIBSTACK *newlibs;
	if ( libcount >= MAXLIB )
	{	error("too many nested libraries");
		return 0;
	}
	else
	{	libcount++;
		newlibs = (LIBSTACK*)as_calloc(1, sizeof(LIBSTACK));
		newlibs->prev = libstk;
		libstk = newlibs;
		libstk->filename = fname;
		libstk->flineno = flineno;
		libstk->slineno = slineno;
		libstk->linetoread = linetoread;
		libstk->stream = srcf;
		libstk->parent = as_strdup(parentlabel);
		libstk->srcname = srcname;
		libstk->srclineno = srclineno;
		libstk->blankline = blankline;
		libstk->namespc = namespc;
		libstk->condstk = condstk;
		libstk->cond = cond;
		libstk->nodp = nodp;
		libstk->globalize = globalize;
		libstk->globltbl = privglobltbl;
		libstk->macrostk = macrostk;
#ifdef	MC6809
		libstk->dp = dp;
#endif
		srcf = fp;
		linetoskip = flineno = lineno;
		linetoread = numline;
		fname = filename;
		src_set(fname);
		slineno = 0;
		srcname = NULL;
		namespc = namspc ? namspc : namespc ? as_strdup(namespc) : NULL;
		condstk = NULL;
		cond = COND_NONE;
		privglobltbl = NULL;
		macrostk = NULL;
		if ( global )
			globalize = global & 1;
		return 1;
	}
}

static
void popfile()
{	LIBSTACK *prevlibs;
#if	WITH_STRUCT
	struct_popfile();
#endif
	macro_popfile();
	cond_free(0);
	globl_freepriv();
	as_free(parentlabel);
	parentlabel = NULL;
	as_free(srcname);
	srcname = NULL;
	if ( srcf != NULL )
	{	if ( srcf != stdin )
			fclose(srcf);
		srcf = NULL;
	}
	if ( libcount <= 0 )
	{	endfile = 1;
		return;
	}
	--libcount;
	as_free(fname);
	fname = libstk->filename;
	src_set(fname);
	flineno = libstk->flineno;
	slineno = libstk->slineno;
	linetoread = libstk->linetoread;
	linetoskip = 0;
	parentlabel = libstk->parent;
	srcname = libstk->srcname;
	srclineno = libstk->srclineno;
	blankline = libstk->blankline;
	as_free(namespc);
	namespc = libstk->namespc;
	condstk = libstk->condstk;
	cond = libstk->cond;
	nodp = libstk->nodp;
	globalize = libstk->globalize;
	privglobltbl = libstk->globltbl;
	macrostk = libstk->macrostk;
#ifdef	MC6809
	dp = libstk->dp;
#endif
	srcf = libstk->stream;
	prevlibs = libstk->prev;
	as_free(libstk);
	libstk = prevlibs;
}

static
int byterange(int b)
{	return -128 <= b && b <= 127;
}

#ifdef	MC6809
static
int checkbyte(int b)
{	int range;
	range = byterange(b);
	if ( byte || (range && valid && !word) ) {
		if ( !range )
			error("signed byte offset exceeds bounds");
		return 1; }
	return 0;
}

static
int checkfivebit(int b)
{	int range;
	range = -16 <= b && b <= 15;
	if ( fivebit || (range && valid && !byte && !word) ) {
		if ( !range )
			error("signed 5-bit offset exceeds bounds");
		return 1; }
	return 0;
}
#endif

static
int checkchar(char c)
{	if ( toupper(*lineptr) == c )
	{	lineptr++;
		return 1;
	}
	return 0;
}

#ifdef	MC6809
static
int getreg(int r)
{	int reg;
	reg = 0;
	if ( checkreg('A') ) reg = A;
	else if ( checkreg('B') ) reg = B;
	else if ( checkreg('D') ) reg = D;
	else if ( checkreg('X') ) reg = X;
	else if ( checkreg('Y') ) reg = Y;
	else if ( checkreg('U') ) reg = U;
	else if ( checkreg('S') ) reg = S;
	else switch ( toupper(*lineptr++) )
	{	case 'C': if ( checkreg('C') ) { reg = CC; } break;
		case 'D': if ( checkreg('P') ) { reg = DP; } break;
		case 'P': if ( checkreg('C') ) { reg = PC;   break; }
			else if ( checkchar('C') ) {
				if ( checkreg('R') ) reg = PCR;
				else lineptr--; }
			break;
		default: lineptr--;
	}
	if ( r & reg ) return reg;
	error("illegal register");
	return 0;
}

static
int checkreg(char r)
{	if ( (toupper(*lineptr++) == r) && !isalnum(*lineptr) ) return 1;
	lineptr--;
	return 0;
}
#endif

static
void putcode(int grp, int mode)
{	if ( oprptr->prefix )
	{	printbyte(putb(oprptr->prefix),LOO);
		printbyte(putb(oprptr->opcode + offset[grp][mode]),LOO+2);
	}
	else printbyte(putb(oprptr->opcode + offset[grp][mode]),LOO);
} 

#ifdef	MC6809
static
void postbyte(int b)
{	postf = 1;
	printbyte(putb(b),LOD);
}
#endif

static
void putbyte(int b)
{	printbyte(putb(b),(postf ? LOD+3 : LOD));
}

static
void putword(int w)
{	printword(putw1(w),(postf ? LOD+3 : LOD));
}

static
void put1byte(int b)
{	if ( pos + 3 > LINEHEAD ) flushline();
	printbyte(putb(b),pos);
	pos += 3;
}

static
void put1word(int w)
{	if ( pos + 5 > LINEHEAD ) flushline();
	printword(putw1(w),pos);
	pos += 5;
}

#ifdef	MC6809
static
void printoptmark()
{	linebuf[LOM + lndigitoff] = '>';
}
#endif

static
void printdecimal(unsigned int n, char *p)
{	char *q;
	if ( pass != 0 ) return;
	q = p + 9 + lndigitoff;
	do
	{	*q-- = (char)(n % 10 + '0');
		n /= 10;
	} while ( n && p <= q );
}

static
void clearaddress()
{	int i, s;
	if ( pass != 0 ) return;
	s = LOA + lndigitoff;
	for ( i = s; i < s+4; i++ ) linebuf[i] = ' ';
}

static
void printaddress(int a)
{	if ( pass != 0 ) return;
	printword(a,LOA);
}

static
void printword(int w, int c)
{	if ( pass != 0 ) return;
	printbyte((w >> 8),c);
	printbyte(w,c+2);
}

static
void printbyte(int b, int c)
{	if ( pass != 0 ) return;
	linebuf[c + lndigitoff + 0] = (char)hexdigit(b >> 4);
	linebuf[c + lndigitoff + 1] = (char)hexdigit(b);
}

static
void printdigit(int d, int c)
{	if ( pass != 0 ) return;
	linebuf[c + lndigitoff] = '0'+d;
}

static
void flushline()
{	char *p;
	putline();
	for ( p = linebuf+lndigit; p < linebuf + LINEHEAD; p++ ) *p = ' ';
	*p++ = '\n';
	*p = '\0';
	printaddress(lc);
	pos = LOO;
}

static
void putline()
{	if ( pass == 0 && list && expand ) {
		fputs(linebuf+lndigit,lstf); }
}

static
void calclndigit()
{	unsigned int digit, number;
	digit = 1;
	number = llineno;
	while (number /= 10)
		digit++;
	if ( digit < 4 )
		digit = 4;
	else if ( digit > 10 )
		digit = 10;
	/* special case when five digit: */
	/* we keep the listing tab aligned to 8 char */
	lndigitoff = digit == 5 ? 1 : 0;
	lndigit = 10-digit+lndigitoff;
}

static
void terminate()
{	if ( !onepass )
	{	if ( totsize != lastsize )
			nchange++;
		if ( pass == 0 )
		{	if ( nchange && errors == 0 )
			{
				fprintf(stderr, "The assembly was still unstabilized at the final pass.\n"
						"The complexity may be too high for the %s-pass assembler mode.\n"
						"%s.\n",
						onepass		? "single" : (optimize ? "multi" : "two"),
						!optimize	? "Please try multi-pass mode with -O1 option" :
								"Possible cause are the use of SET where EQU should be used or\nunstable circular reference");
				errors++;
			}
		}
	}
	if ( verbos > 1 )
		fprintf(stderr, "%d byte%s, delta %d\n",totsize,totsize>1?"s":"",totsize-lastsize);
DEBUG(fprintf(stderr, "lastsize=%d totsize=%d\n",lastsize,totsize);)
	lastsize = totsize;
	if ( pass != 0 )
		return;
	if ( object ) termobj();
	if ( symbol >= 0 ) dumpsymbol();
	printlog();
}

static
void addincdir(const char *path)
{
	INCDIR *id;
	id = (INCDIR*)as_calloc(1, sizeof(INCDIR));
	if ( incdir == NULL )
		incdir = incdirtail = id;
	else
		incdirtail = incdirtail->next = id;
	id->path = path;
	if ( verbos > 1 )
		fprintf(stderr,"include path: %s\n",path);
}

#ifdef	CLEANUP
static
void cleanup()
{
	INCDIR *next;
	while ( libcount > 0 )
		popfile();
	popfile();
	as_free(fname);
	while ( incdir )
	{	next = incdir->next;
		as_free(incdir);
		incdir = next;
	}
	as_free(reflbl);
	freenode(label);
	src_free(srctab);
	macro_cleanup();
	globl_cleanup();
#if	WITH_STRUCT
	struct_cleanup();
#endif
	if ( objarg )
		as_free(objfile);
	if ( symarg )
		as_free(symfile);
	if ( lstarg )
		as_free(lstfile);
}
#endif

static
void printlogpass(FILE* f)
{
	fprintf(f,"     Total Passes %d\n",totpass);
}

static
void printlog()
{	if ( pass != 0 ) return;
	if ( list ) {
		if ( errors ) putc('\n',lstf);
		fprintf(lstf,"     Total Labels %d\n",labels);
		printlogpass(lstf);
#ifdef	MC6809
		if ( def_optlongbranch || optlongbranch || optbrcnt ) fprintf(lstf,"     Total Optimized Branch %d\n",optbrcnt);
#endif
		fprintf(lstf,"     Total Errors %d\n",errors);
	}
	if ( verbos || !quiet ) {
		if ( errors || verbos > 1 ) putc('\n',stderr);
		if ( verbos ) {
			fprintf(stderr,"     Total Labels %d\n",labels);
			printlogpass(stderr);
#ifdef	MC6809
			if ( def_optlongbranch || optlongbranch || optbrcnt ) fprintf(stderr,"     Total Optimized Branch %d\n",optbrcnt);
#endif
		}
		fprintf(stderr,"     Total Errors %d\n",errors);
	}
}

static
void dumpsymbol()
{	tsym = 0;
	fprintf(symf,"* Symbol Table Level %d\n",symbol);
	if ( srcfile )
		fprintf(symf,"* Source File: %s\n",srcfile);
	if ( lstfile && lstf != symf )
		fprintf(symf,"* Listing File: %s\n",lstfile);
	printnode(label);
	fprintf(symf,"* Total Symbols %d\n",tsym);
}

static
void printnode(LBLTABLE *lp)
{	if ( lp == NULL ) return;
	printnode((LBLTABLE*)lp->t.right);
	if ( lp->t.name && lp->level >= symbol && lp->type != SYM_REFERENCED ) {
		int minus = lp->value < 0;
		int value = minus ? -lp->value : lp->value;
		if ( !list || !lp->lline )
			fprintf(symf,"%s equ %s$%X\n",lp->t.name,minus?"-":"",value);
		else
			fprintf(symf,"%s equ %s$%X ; %d\n",lp->t.name,minus?"-":"",value,lp->lline);
		tsym++; }
	printnode((LBLTABLE*)lp->t.left);
}

static
void printtitle(int notice)
{
	if ( quiet )
		return;
#if	!RELEASE && defined(REVISION)
	fputs(TITLE " version " VERSION " (" REVISION ")\n\n",stderr);
#else
	fputs(TITLE " version " VERSION "\n\n",stderr);
#endif
	if ( notice )
		fputs("This program is free software; you may redistribute it under the terms of\n"
		      "the GNU General Public License.  This program has ABSOLUTELY NO WARRANTY.\n\n",stderr);
}

static
void printusage(char *name)
{
	fprintf(stderr,"Usage: %s src_file [-O[level]] "
#ifdef	MC6809
		"[-B] "
#else
		"[-X] "
#endif
		"[-e] "
#if	RAW_FORMAT || IHEX_FORMAT || DECB_FORMAT
		"[-f format] "
#endif
		"[-p] [-q] [-u] [-v] [-x] [-D sym[=val]] [-I dir] "
#ifdef	SYSINCDIR
		"[-N] "
#endif
		"[-P option] "
		"[-o [obj_file]] [-l [lst_file]] [-s[level] [sym_file]]\n",name);
}
