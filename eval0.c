/*
 * Copyright (C) 1981, 1987 Masataka Ohta, Hiroshi Tezuka
 * Copyright (C) 2016-2019 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  eval0.c --- evaluate expression with precedence
 *
 */

/*
http://en.wikipedia.org/wiki/Operator-precedence_parser

parse_expression ()
    return parse_expression_1 (parse_primary (), 0)

parse_expression_1 (lhs, min_precedence)
    lookahead := peek next token
    while lookahead is a binary operator whose precedence is >= min_precedence
        op := lookahead
        advance to next token
        rhs := parse_primary ()
        lookahead := peek next token
        while lookahead is a binary operator whose precedence is greater
                 than op's, or a right-associative operator
                 whose precedence is equal to op's
            rhs := parse_expression_1 (rhs, lookahead's precedence)
            lookahead := peek next token
        lhs := the result of applying op with operands lhs and rhs
    return lhs
*/

static
int precedence(int op)
{	switch ( op )
	{	/* Priorities order are the same as C.    */
		/* From lower to higher priority:         */
		case '|': case '!': return 1; /* 1  bitwise OR      */
		case '^': return 2; /* 2  bitwise XOR     */
		case '&': return 3; /* 3  bitwise AND     */
		case '<':           /* 4  left shift      */
		case '>': return 4; /* 4  right shift     */
		case '+':           /* 5  addition        */
		case '-': return 5; /* 5  subtraction     */
		case '*':           /* 6  multiplication  */
		case '/':           /* 6  division        */
		case '%': return 6; /* 6  remainder       */
		/* Delimiters, same as isspace():         */
		case ' ': case '\t': case '\f': /*        */
		/* Delimiters, others:                    */
		case ',': case ')': case ']': /*          */
		case EOL: return -1;
	}
	error("illegal character in expression");
	experr = 1;
DEBUG(fprintf(stderr,"*lineptr : %c\n",*lineptr);)
	return -1;
}

static
int apply(int op, int lhs, int rhs)
{	switch ( op )
	{	case '|': case '!': return lhs | rhs;
		case '^': return lhs ^ rhs;
		case '&': return lhs & rhs;
		case '<': return lhs << rhs;
		case '>': return lhs >> rhs;
		case '+': return lhs + rhs;
		case '-': return lhs - rhs;
		case '*': return lhs * rhs;
		case '/':
			if ( rhs == 0 ) goto divby0;
			return lhs / rhs;
		case '%':
			if ( rhs == 0 ) goto divby0;
			return lhs % rhs;
	}
	return lhs;
divby0:
	error("division by zero");
	experr = 1;
	return -1;
}

static
int parse(int lhs, int min_precedence)
{	int lookahead, lookaheadpre, op, oppre, rhs;
	lookahead = *lineptr;
	lookaheadpre = precedence(lookahead);
	while ( lookaheadpre >= min_precedence ) {
		op = lookahead;
		oppre = lookaheadpre;
		switch ( *lineptr )
		{	case ' ': case '\t': case '\f': /* same as isspace() */
			case ',': case ')': case ']':
			case EOL:
				return lhs;
		}
		lineptr++;
		rhs = term();
		lookahead = *lineptr;
		lookaheadpre = precedence(lookahead);
		while ( lookaheadpre > oppre ) {
			rhs = parse(rhs, lookaheadpre);
			lookahead = *lineptr;
			lookaheadpre = precedence(lookahead);
		}
		lhs = apply(op, lhs, rhs);
	}
	return lhs;
}

static
int expression0()
{	int val;
#ifdef	MC6809
	int chk5bit;
#endif
	if ( !expdepth ) {
		experr = 0;
		valid = 1;
		byte = word = 0;
#ifdef	MC6809
		chk5bit = fivebit;
		fivebit = 0;
#endif
		if ( checkchar('<') ) {
			byte = 1;
#ifdef	MC6809
			if ( chk5bit && checkchar('<') )
				fivebit = 1;
#endif
		}
		else if ( checkchar('>') ) word = 1;
	}
	expdepth++;
	val = parse(term(), 0);
	expdepth--;
	return val;
}
