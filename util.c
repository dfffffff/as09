/*
 * Copyright (C) 1981, 1987 Masataka Ohta, Hiroshi Tezuka
 * Copyright (C) 2016-2021 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  util.c --- utilities
 *
 */

static
int hexdigit(int x)
{	return ( (x &= 0x0f) < 10 ? x + '0' : x - 10 + 'A');
}

static
int isspace(int c)
{	return (c == ' ' || c == '\t' || c == '\f');
}

static
int isseparator(int c)
{	return (c == ' ' || c == '\t' || c == '\f' || c == EOL);
}

static
int isalnum(int c)
{	return (('A' <= c && c <= 'Z') || ('a' <= c && c <= 'z')
			|| (c == '.') || (c == '_') || (c == '?') || (c == '@')
			|| (c == '$') || ('0' <= c && c <= '9'));
}

static
int isalpha(int c)
{	return (('A' <= c && c <= 'Z') || ('a' <= c && c <= 'z')
			|| (c == '.') || (c == '_') || (c == '?'));
}

static
int isalphanumeric(int c)
{	return (('A' <= c && c <= 'Z') || ('a' <= c && c <= 'z')
			|| ('0' <= c && c <= '9'));
}

static
int isdigit(int c)
{	return ('0' <= c && c <= '9');
}

static
int isxdigit(int c)
{	return (isdigit(c) ||
		('A' <= c && c <= 'F') || ('a' <= c && c <= 'f'));
}

static
int iscomment(int c)
{	return (c == EOL || c == '*' || c == ';' || c == '#');
}

static
int toupper(int c)
{	if ( 'a' <= c && c <= 'z' )
		return c & 0xdf;
	return c;
}

static
void toupperstr(char *str)
{	for ( ; *str; str++ )
		*str = toupper(*str);
}

static
int alphanumtoint(int c)
{	if (c >= '0' && c <= '9')
		c = c - '0';
	else if (c >= 'A' && c <= 'Z')
		c = c - 'A' + 10;
	else if (c >= 'a' && c <= 'z')
		c = c - 'a' + 10 + 26;
	else
		c = -1;
	return c;
}

static
char *make_uint(char *buffer, unsigned int num)
{
	char *ptr, c;
	ptr = buffer + (MAKEUINT_BUF-1);
	*ptr = 0;
	do {
		c = num % 10;
		num /= 10;
		*--ptr = c + '0';
	} while (num);
	return ptr;
}

static
int as_strcmpi(const char *str1, const char *str2)
{
	unsigned char a, b;
	do {
		a = (unsigned char)toupper(*str1++);
		b = (unsigned char)toupper(*str2++);
		if (a < b)
			return -1;
		if (a > b)
			return 1;
	} while (a);
	return 0;
}

#if	WITH_SIZEOF || WITH_DEFINED
static
int as_strncmpi(const char *str1, const char *str2, int len)
{
	unsigned char a, b;
	do {
		if (!len)
			break;
		len--;
		a = (unsigned char)toupper(*str1++);
		b = (unsigned char)toupper(*str2++);
		if (a < b)
			return -1;
		if (a > b)
			return 1;
	} while (a);
	return 0;
}
#endif
