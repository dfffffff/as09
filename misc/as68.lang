<?xml version="1.0" encoding="UTF-8"?>
<!--

 This file is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

-->

<!-- gtksourceview language-specs -->
<!-- syntax definition for as68 assembler -->
<language id="as68" _name="As68" version="2.0" _section="Sources">
  <metadata>
    <property name="mimetypes">text/x-asm;text/x-assembler</property>
    <property name="globs">*.a68;*.i68;*.s68;*.A68;*.I68;*.S68</property>
  </metadata>

  <styles>
    <style id="comment"           _name="Comment"               map-to="def:comment"/>
    <style id="instruction"       _name="Instruction"           map-to="def:keyword"/>
    <style id="directive"         _name="Directive"             map-to="c:preprocessor"/>
    <style id="string"            _name="String"                map-to="def:string"/>
    <style id="decimal"           _name="Decimal number"        map-to="def:decimal"/>
    <style id="base-n-integer"    _name="Base-N number"         map-to="def:base-n-integer"/>
    <style id="label"             _name="Label"                 map-to="def:preprocessor"/>
    <style id="badlabel"          _name="Bad Label"             map-to="def:error"/>
    <style id="register"          _name="Register"              map-to="def:character"/>
    <style id="operand"           _name="Operand"               map-to="def:type"/>
  </styles>

  <default-regex-options case-sensitive="false"/>

  <definitions>

    <context id="as68" class="no-spell-check">
      <include>

        <context id="comment1" style-ref="comment">
          <match extended="true">^[*]+.*</match>
        </context>

        <context id="comment2" style-ref="comment">
          <match extended="true">^[#]+.*</match>
        </context>

        <context id="comment3" style-ref="comment">
          <match extended="true">;.*</match>
        </context>

        <context id="badlabel" style-ref="badlabel">
          <match>^[^._A-Z\s]+[^\s]*</match>
        </context>

        <context id="label" style-ref="label">
          <match>^[^0-9\s]*[._A-Z]*[._A-Z0-9]+</match>
        </context>

        <context id="label2" style-ref="label">
          <match>[A-Z]*\.[A-Z]*</match>
        </context>

        <context id="directive" style-ref="directive">
          <keyword>BIN</keyword>
          <keyword>ELDEF</keyword>
          <keyword>ELIFC</keyword>
          <keyword>ELIFEQ</keyword>
          <keyword>ELIFGE</keyword>
          <keyword>ELIFGT</keyword>
          <keyword>ELIFLE</keyword>
          <keyword>ELIFLT</keyword>
          <keyword>ELIFNC</keyword>
          <keyword>ELIFNE</keyword>
          <keyword>ELNDEF</keyword>
          <keyword>ELNREF</keyword>
          <keyword>ELREF</keyword>
          <keyword>ELSE</keyword>
          <keyword>END</keyword>
          <keyword>ENDC</keyword>
          <keyword>ENDM</keyword>
          <keyword>ENDS</keyword>
          <keyword>EQU</keyword>
          <keyword>ERR</keyword>
          <keyword>EXIT</keyword>
          <keyword>FCB</keyword>
          <keyword>FCC</keyword>
          <keyword>FCN</keyword>
          <keyword>FCS</keyword>
          <keyword>FDB</keyword>
          <keyword>FLB</keyword>
          <keyword>FLD</keyword>
          <keyword>GLOBL</keyword>
          <keyword>IFC</keyword>
          <keyword>IFDEF</keyword>
          <keyword>IFEQ</keyword>
          <keyword>IFGE</keyword>
          <keyword>IFGT</keyword>
          <keyword>IFLE</keyword>
          <keyword>IFLT</keyword>
          <keyword>IFNC</keyword>
          <keyword>IFNDEF</keyword>
          <keyword>IFNE</keyword>
          <keyword>IFNREF</keyword>
          <keyword>IFP1</keyword>
          <keyword>IFP2</keyword>
          <keyword>IFREF</keyword>
          <keyword>LIB</keyword>
          <keyword>MACR</keyword>
          <keyword>NAM</keyword>
          <keyword>NAME</keyword>
          <keyword>NIL</keyword>
          <keyword>NODP</keyword>
          <keyword>OPT</keyword>
          <keyword>ORG</keyword>
          <keyword>PAG</keyword>
          <keyword>PAGE</keyword>
          <keyword>REF</keyword>
          <keyword>RMB</keyword>
          <keyword>SET</keyword>
          <keyword>SPC</keyword>
          <keyword>STRUCT</keyword>
          <keyword>STTL</keyword>
          <keyword>TTL</keyword>
          <keyword>WITH</keyword>
          <keyword>XDEF</keyword>
          <keyword>XREF</keyword>
          <keyword>ZMB</keyword>
        </context>

        <context id="instruction" style-ref="instruction">
          <keyword>ABA</keyword>
          <keyword>ADCA</keyword>
          <keyword>ADCB</keyword>
          <keyword>ADDA</keyword>
          <keyword>ADDB</keyword>
          <keyword>ANDA</keyword>
          <keyword>ANDB</keyword>
          <keyword>ASL</keyword>
          <keyword>ASLA</keyword>
          <keyword>ASLB</keyword>
          <keyword>ASR</keyword>
          <keyword>ASRA</keyword>
          <keyword>ASRB</keyword>
          <keyword>BCC</keyword>
          <keyword>BCS</keyword>
          <keyword>BEQ</keyword>
          <keyword>BGE</keyword>
          <keyword>BGT</keyword>
          <keyword>BHI</keyword>
          <keyword>BITA</keyword>
          <keyword>BITB</keyword>
          <keyword>BLE</keyword>
          <keyword>BLS</keyword>
          <keyword>BLT</keyword>
          <keyword>BMI</keyword>
          <keyword>BNE</keyword>
          <keyword>BPL</keyword>
          <keyword>BRA</keyword>
          <keyword>BSR</keyword>
          <keyword>BVC</keyword>
          <keyword>BVS</keyword>
          <keyword>CBA</keyword>
          <keyword>CLC</keyword>
          <keyword>CLI</keyword>
          <keyword>CLR</keyword>
          <keyword>CLRA</keyword>
          <keyword>CLRB</keyword>
          <keyword>CLV</keyword>
          <keyword>CMPA</keyword>
          <keyword>CMPB</keyword>
          <keyword>COM</keyword>
          <keyword>COMA</keyword>
          <keyword>COMB</keyword>
          <keyword>CPX</keyword>
          <keyword>DAA</keyword>
          <keyword>DEC</keyword>
          <keyword>DECA</keyword>
          <keyword>DECB</keyword>
          <keyword>DES</keyword>
          <keyword>DEX</keyword>
          <keyword>EORA</keyword>
          <keyword>EORB</keyword>
          <keyword>INC</keyword>
          <keyword>INCA</keyword>
          <keyword>INCB</keyword>
          <keyword>INS</keyword>
          <keyword>INX</keyword>
          <keyword>JMP</keyword>
          <keyword>JSR</keyword>
          <keyword>LDAA</keyword>
          <keyword>LDAB</keyword>
          <keyword>LDS</keyword>
          <keyword>LDX</keyword>
          <keyword>LSR</keyword>
          <keyword>LSRA</keyword>
          <keyword>LSRB</keyword>
          <keyword>NEG</keyword>
          <keyword>NEGA</keyword>
          <keyword>NEGB</keyword>
          <keyword>NOP</keyword>
          <keyword>ORAA</keyword>
          <keyword>ORAB</keyword>
          <keyword>PSHA</keyword>
          <keyword>PSHB</keyword>
          <keyword>PULA</keyword>
          <keyword>PULB</keyword>
          <keyword>ROL</keyword>
          <keyword>ROLA</keyword>
          <keyword>ROLB</keyword>
          <keyword>ROR</keyword>
          <keyword>RORA</keyword>
          <keyword>RORB</keyword>
          <keyword>RTI</keyword>
          <keyword>RTS</keyword>
          <keyword>SBA</keyword>
          <keyword>SBCA</keyword>
          <keyword>SBCB</keyword>
          <keyword>SEC</keyword>
          <keyword>SEI</keyword>
          <keyword>SEV</keyword>
          <keyword>STAA</keyword>
          <keyword>STAB</keyword>
          <keyword>STS</keyword>
          <keyword>STX</keyword>
          <keyword>SUBA</keyword>
          <keyword>SUBB</keyword>
          <keyword>SWI</keyword>
          <keyword>TAB</keyword>
          <keyword>TAP</keyword>
          <keyword>TBA</keyword>
          <keyword>TPA</keyword>
          <keyword>TST</keyword>
          <keyword>TSTA</keyword>
          <keyword>TSTB</keyword>
          <keyword>TSX</keyword>
          <keyword>TXS</keyword>
          <keyword>WAI</keyword>
        </context>

        <context id="register" style-ref="register">
          <keyword>X</keyword>
        </context>

        <context id="filename" style-ref="string" class="string">
          <match>\s(&lt;.*&gt;)</match>
        </context>

        <context id="operand1" style-ref="operand">
<!--          <match>\s(#|&lt;|&gt;|\^|\[&lt;|\[&gt;|\[)</match>-->
          <match>\s(#|&lt;&lt;|&lt;|&gt;|\[&lt;|\[&gt;|\[)</match>
        </context>

        <context id="operand2" style-ref="operand">
          <match>\]([\s]+|$)</match>
        </context>

        <context id="decimal" style-ref="decimal">
          <match>(?&lt;![\w\.])([1-9][0-9]*|0)(?![\w\.])</match>
        </context>

        <context id="octal" style-ref="base-n-integer">
          <match>(?&lt;![\w\.])[@][0-7]+(?![\w\.])</match>
        </context>

        <context id="hex" style-ref="base-n-integer">
          <match>(?&lt;![\w\.])[$][0-9A-F]+(?![\w\.])</match>
        </context>

        <context id="binary" style-ref="base-n-integer">
          <match>(?&lt;![\w\.])[%][0-1]+(?![\w\.])</match>
        </context>

        <context id="ascii" style-ref="base-n-integer">
          <match>(?&lt;![\w\.])['].(?![\w\.])</match>
        </context>

        <context id="double-quoted-string" style-ref="string" end-at-line-end="true" class="string">
          <start>"</start>
          <end>"</end>
          <include>
            <context ref="def:line-continue"/>
          </include>
        </context>

      </include>
    </context>
  </definitions>
</language>
