#########################################################
#                                                       #
#       MC6800/6801/6803/6809 cross assembler           #
#                                                       #
#        process this file with GNU Make                #
#                                                       #
#########################################################

## WIN32 i686
#CC = i686-w64-mingw32-gcc-win32
#STRIP = i686-w64-mingw32-strip
#BINSUFFIX = .exe
## WIN32 x86_64
#CC = x86_64-w64-mingw32-gcc-win32
#STRIP = x86_64-w64-mingw32-strip
#BINSUFFIX = .exe

#CFLAGS = -O
#CFLAGS = -Wall -Wextra -pedantic -ansi -g -DDEBUG
CFLAGS = -Wall -Wextra -pedantic -ansi -O2
#CFLAGS = -Wall -Wextra -pedantic -O2
#CFLAGS += -DCLEANUP
#CFLAGS += -DDEBUG
CFLAGS += $(EXTRA_CFLAGS)

## MC6800/6801/6803 cross assembler binary name
AS68 = as68$(BINSUFFIX)

## MC6809 cross assembler binary name
AS09 = as09$(BINSUFFIX)

## installation prefix
PREFIX = /usr/local

## installation directory
DESTDIR = $(PREFIX)/bin

## system include directory
INCDIR = $(PREFIX)/share/
INCDIR68 = $(INCDIR)as68
INCDIR09 = $(INCDIR)as09

## superuser utility
SUDO ?= sudo

## strip utility
STRIP ?= strip

## revision number
REVISION = $(shell test -f revnum && printf %s -DREVISION=\\\"\"git~$$(cat revnum)\"\\\" || printf %s -UREVISION)

#########################################################

ifneq ($(INCDIR),)
	SYSINCDIR68 = -DSYSINCDIR="\"$(INCDIR68)\""
	SYSINCDIR09 = -DSYSINCDIR="\"$(INCDIR09)\""
else
	SYSINCDIR68 =
	SYSINCDIR09 =
endif

all: nostrip
	$(STRIP) $(AS68)
	$(STRIP) $(AS09)

nostrip: $(AS68) $(AS09)

bin:
	@echo "building: linux i386"
	$(MAKE) clean all move \
		CC=x86_64-linux-gnu-gcc \
		STRIP=x86_64-linux-gnu-strip \
		BINSUFFIX= \
		EXTRA_CFLAGS="-m32 -march=i386 -mtune=generic -DCOMPAT_BIN -Wl,--hash-style=both" \
		DEST=bin/linux/i386/
	@echo "building: linux x86_64"
	$(MAKE) clean all move \
		CC=x86_64-linux-gnu-gcc \
		STRIP=x86_64-linux-gnu-strip \
		BINSUFFIX= \
		EXTRA_CFLAGS="-m64 -march=x86-64 -mtune=generic -DCOMPAT_BIN -Wl,--hash-style=both" \
		DEST=bin/linux/x86_64/
	@echo "building: win32 i386"
	$(MAKE) clean all move \
		CC=i686-w64-mingw32-gcc-win32 \
		STRIP=i686-w64-mingw32-strip \
		BINSUFFIX=.exe \
		EXTRA_CFLAGS="-m32 -march=i386 -mtune=generic -DCOMPAT_BIN" \
		DEST=bin/win32/i386/ \
		INCDIR=
	@echo "building: win32 x86_64"
	$(MAKE) clean all move \
		CC=x86_64-w64-mingw32-gcc-win32 \
		STRIP=x86_64-w64-mingw32-strip \
		BINSUFFIX=.exe \
		EXTRA_CFLAGS="-m64 -march=x86-64 -mtune=generic -DCOMPAT_BIN" \
		DEST=bin/win32/x86_64/ \
		INCDIR=

$(AS68): as68.c as68.h revnum
	$(CC) $(CFLAGS) $(REVISION) $(SYSINCDIR68) -o $@ as68.c

$(AS09): as68.c as68.h revnum
	$(CC) $(CFLAGS) $(REVISION) $(SYSINCDIR09) -DMC6809 -o $@ as68.c

clean:
	rm -f -- $(AS68) $(AS09) revnum

distclean: clean
	rm -f -- bin/*/*/as*

install:
	$(SUDO) install -p -- $(AS68) $(DESTDIR)/
	$(SUDO) install -p -- $(AS09) $(DESTDIR)/

uninstall:
	$(SUDO) rm -f -- $(DESTDIR)/$(AS68)
	$(SUDO) rm -f -- $(DESTDIR)/$(AS09)

move:
	mv -- $(AS68) $(AS09) $(DEST)

revnum:
	git rev-list --count --first-parent HEAD 2>/dev/null >$@ || rm -f -- $@

cxxtest:
	$(CXX) $(CFLAGS) $(SYSINCDIR68) -DREVISION=\"1\" as68.c -o /dev/null
	$(CXX) $(CFLAGS) $(SYSINCDIR09) -DREVISION=\"1\" -DMC6809 as68.c -o /dev/null

as68.nanorc as09.nanorc:
	$(SUDO) cp misc/$@ /usr/share/nano/

as68.lang as09.lang:
	$(SUDO) cp misc/$@ /usr/share/gtksourceview-3.0/language-specs/

install_syntax: as68.nanorc as09.nanorc as68.lang as09.lang

.PHONY: $(AS68) $(AS09) all nostrip bin clean install uninstall move revnum
.PHONY: cxxtest as68.nanorc as68.lang as09.nanorc as09.lang install_syntax
