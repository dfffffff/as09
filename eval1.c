/*
 * Copyright (C) 1981, 1987 Masataka Ohta, Hiroshi Tezuka
 * Copyright (C) 2016-2019 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  eval1.c --- evaluate expression without precedence
 *
 */

static
int expression1()
{	int val, tempval;
#ifdef	MC6809
	int chk5bit;
#endif
	if ( !expdepth ) {
		experr = 0;
		valid = 1;
		byte = word = 0;
#ifdef	MC6809
		chk5bit = fivebit;
		fivebit = 0;
#endif
		if ( checkchar('<') ) {
			byte = 1;
#ifdef	MC6809
			if ( chk5bit && checkchar('<') )
				fivebit = 1;
#endif
		}
		else if ( checkchar('>') ) word = 1;
	}
	expdepth++;
	val = term();
	while ( 1 )
	{	switch ( *lineptr++ )
		{	case '+': val += term(); continue;
			case '-': val -= term(); continue;
			case '*': val *= term(); continue;
			case '/': tempval = term(); if ( tempval == 0 ) { error("division by zero"); experr = 1; val = -1; } else val /= tempval; continue;
			case '%': tempval = term(); if ( tempval == 0 ) { error("division by zero"); experr = 1; val = -1; } else val %= tempval; continue;
			case '&': val &= term(); continue;
			case '|': val |= term(); continue;
			case '^': val ^= term(); continue;
			case '<': val <<= term(); continue;
			case '>': val >>= term(); continue;
			default: break;
		}
		break;
	}
	switch ( *--lineptr )
	{	case ' ': case '\t': case '\f': /* same as isspace() */
		case ',': case ')': case ']':
		case EOL:
			break;
		default:
			error("illegal character in expression");
			experr = 1;
DEBUG(fprintf(stderr,"*lineptr : %c\n",*lineptr);)
	}
	expdepth--;
	return val;
}
