/*
 * Copyright (C) 2021 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  features.h --- features selection
 *
 */

/* turn off all features if defined */
#ifdef FEATURES_OFF
#define WITH_ATLABEL	0
#define WITH_DEFINED	0
#define WITH_EXTRA	0
#define WITH_IFENDIF	0
#define WITH_OS9	0
#define WITH_SIZEOF	0
#define WITH_STRUCT	0
#define WITH_TIME	0
#define WITH_WEIRDCONST	0
#endif

/* enable label begining with @ */
#ifndef WITH_ATLABEL
#define WITH_ATLABEL	1
#endif

/* enable defined{} operator */
#ifndef WITH_DEFINED
#define WITH_DEFINED	1
#endif

/* enable fill/fzb/use ops */
#ifndef WITH_EXTRA
#define WITH_EXTRA	1
#endif

/* enable if/endif ops */
#ifndef WITH_IFENDIF
#define WITH_IFENDIF	1
#endif

/* enable mod/emod/os9 ops */
#ifndef WITH_OS9
#ifndef MC6809
#define WITH_OS9	0
#else
#define WITH_OS9	1
#endif
#endif

/* enable sizeof{} operator */
#ifndef WITH_SIZEOF
#define WITH_SIZEOF	1
#endif

/* enable struct/ends ops */
#ifndef WITH_STRUCT
#define WITH_STRUCT	1
#endif

/* enable dtb/dts ops */
#ifndef WITH_TIME
#define WITH_TIME	1
#endif

/* enable minus after the constant type character */
#ifndef WITH_WEIRDCONST
#define WITH_WEIRDCONST	1
#endif

