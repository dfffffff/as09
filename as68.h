/*
 * Copyright (C) 1981, 1987 Masataka Ohta, Hiroshi Tezuka
 * Copyright (C) 2016-2021 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  as68.h --- main header
 *
 */

/* features selection */
#include "features.h"

#define	RELEASE		0
#define	VERSION		"1.5.0"

#ifdef	MC6809
#define	TITLE		"MC6809 cross assembler"
#else
#define	TITLE		"MC6800/6801/6803 cross assembler"
#endif

#undef	DEBUGOPT
#undef	NDEBUG
#ifdef	DEBUG
#undef	DEBUG
#define	DEBUG(X)	if (debug) {X}
#define	NDEBUG(X)	if (!debug) {X}
#define	DEBUGOPT
#else
#define	DEBUG(X)
#define	NDEBUG(X)	X
#endif

#define	MC6801		1

/* dot prefixed directive */
/* 0 : disabled     */
/* 1 : optional     */
/* 2 : mandatory    */
#define	DOTDIR		1

/* comment out to disable support */
#define	SREC_FORMAT	0	/* motorola s-record format */
#define	RAW_FORMAT	1	/* raw binary format */
#define	IHEX_FORMAT	2	/* intel hex format */
#define	DECB_FORMAT	3	/* decb binary format */

/* error print format */
#define	FILELINE	"%s(%d): "
#define	FILELINENOLN	"%s: "

/* end of line char */
#define	EOL		'\n'
#define	EOLS		"\n"

/* path separator char */
#define	PATHSEPCHR	'/'
#define	PATHSEPSTR	"/"

/* error exit code */
#define	GENERROR	1	/* generic */
#define	FIOERROR	2	/* file i/o */
#define	LTLERROR	3	/* line too long */
#define	ARGERROR	4	/* wrong argument */
#define	OPTERROR	5	/* illegal option */
#define	OOMERROR	63	/* out of memory */

/* maximum number of passes */
#define	MAXPASS		100

/* buffer size */
#define	MAXLINESZ	1024
#define	MNEMOSIZE	6
#define	LINEHEAD	(10+20)
#define	MAXCHAR		(LINEHEAD+MAXLINESZ)
#define	OBJSIZE		32
#define	MAXLIB		50	/* maximum library depth */
#define	MNEMOBUFSIZE	(MNEMOSIZE+10)

/* listing line offset */
#define	LOM		10	/* mark */
#define	LOA		(LOM+1)	/* address */
#define	LOO		(LOA+5)	/* opcode */
#define	LOD		(LOO+5)	/* data */

/* register notation */
#define	NONE		0
#define	CC		0x01
#define	A		0x02
#define	B		0x04
#define	D		0x06
#define	DP		0x08
#define	X		0x10
#define	Y		0x20
#define	U		0x40
#define	S		0x80
#define	PC		0x100
#define	PCR		0x200
#define	INDEXREG	(X|Y|U|S)
#define	ALLREG		(CC|A|B|D|DP|X|Y|U|S|PC)

/* addressing mode */
#define	IMMEDIATE	0x01
#define	IMMEDIATE2	0x02
#define	DIRECT		0x04
#define	INDEX		0x08
#define	EXTEND		0x10
#define	LOAD		(IMMEDIATE|DIRECT|INDEX|EXTEND)
#define	LOAD2		(IMMEDIATE2|DIRECT|INDEX|EXTEND)
#define	STORE		(DIRECT|INDEX|EXTEND)
#ifdef	MC6809
#define	MEMORY		(DIRECT|INDEX|EXTEND)
#else
#define	MEMORY		(INDEX|EXTEND)
#endif

/* addressing mode variation group */
#define	GROUP0		0
#define	GROUP1		1
#define	GROUP2		2

/* mode offset */
#define	NO_MODE		0
#define	IMMEDIATE_MODE	0
#define	DIRECT_MODE	1
#define	INDEX_MODE	2
#define	EXTEND_MODE	3

/* symbol type */
#define	SYM_REFERENCED	0 /* symbol created from expression reference */
#define	SYM_DEFINE	1 /* defined from command line */
#define	SYM_LABEL	2 /* defined with label at line start */
#define	SYM_EQUATE	3 /* defined with EQU directive */
#define	SYM_SET		4 /* defined with SET directive */

/* reflabel param */
#define	REF_EXPRESSION	0
#define	REF_DEFINED	1
#define	REF_REFERENCED	2

/* optab.c prefix */
#define	NOLABEL_MASK	0x81 /* no default label mask */
#define	COND		0xFF /* opcode is a conditional directive */
#define	DIR		0xFE /* opcode is a generic directive */
#define	DIRN		0xFD /* opcode is a generic directive, without label */
#define	MACR		0xFB /* opcode is macr directive without label */
#define	OKSD		0xFA /* opcode is a directive allowed in struct */
#if	WITH_STRUCT
#define	ENDS		0xF9 /* opcode is ends directive */
#define	STRC		0xF8 /* opcode is a structure */
#define	STRD		0xF7 /* opcode is struct directive */
#endif
#define	DATA		0xBE /* opcode is a data directive */
#define	RMB		0xBC /* opcode is rmb directive */

/* undefined symbol macro */
#define	UNDEFINED_SYMBOL() do { if (!undef) error(reflbl ? "undefined symbol %s" : "undefined location counter", reflbl); } while (0)

/* symbol search */
#define FIND_SYMBOL(X,Y,Z) (LBLTABLE*)findsymbol((X),(TREE**)(Y),(TREE***)(Z))
#define FIND_GLOBL(X,Y,Z) (GLOBLTABLE*)findsymbol((X),(TREE**)(Y),(TREE***)(Z))

#if	defined(WITH_SIZEOF) || defined(WITH_STRUCT)
#define	SIZEOF_TAG	".?"
#endif

/* cleanup macro */
#ifdef	CLEANUP
#define	IF_CLEANUP(X) X
#else
#define	IF_CLEANUP(X)
#endif

/* util.c */
#define MAKEUINT_BUF (sizeof(unsigned int)*8000/3321+1+1)

/* opcode table */
typedef struct opt {
		char mnemonic[MNEMOSIZE + 1];
		unsigned char prefix;
		unsigned char opcode;
#ifndef	MC6809
		unsigned char option;
#endif
		void (*process)();
		struct opt *nl;
} OPTABLE;

/* binary tree */
#include "tree.h"

/* global symbol */
#include "globl.h"

/* source table */
typedef struct st {
	TREE t;
	unsigned int index;
	unsigned int instance;
} SRCTABLE;

/* label table */
typedef struct lt {
		TREE t;
		SRCTABLE *source;	/* source file */
		int fline;		/* line number in source file */
		int sline;		/* sub line number (macro/struct) */
		int lline;		/* line number in listing */
		int value;		/* symbol value */
		int pass;		/* pass of last definition */
		int rpass;		/* pass of last reference */
		unsigned char valid;	/* symbol is valid, no undef ref */
		unsigned char type;	/* symbol type, SYM_* */
		unsigned char level;	/* symbol level, for '-s' option */
		unsigned char man;	/* label is managed by the op */
		unsigned char inp1;	/* inside IFP1/ENDC block */
} LBLTABLE;

/* conditional */
#include "cond.h"

/* macro */
#include "macro.h"

/* library stack */
typedef struct libs {
		struct libs *prev;
		CONDSTACK *condstk;
		GLOBLTABLE *globltbl;
		MACROSTACK *macrostk;
		char *filename;
		FILE *stream;
		char *parent;
		char *srcname;
		char *namespc;
		int flineno;
		int slineno;
		int linetoread;
		int srclineno;
		int blankline;
		char cond;
		char nodp;
		char globalize;
#ifdef	MC6809
		int dp;
#endif
} LIBSTACK;

/* include directory */
typedef struct id {
		struct id *next;
		const char *path;
} INCDIR;

/* structure */
#if	WITH_STRUCT
#include "struct.h"
#endif

/* variable */
/* main.c */
static int errors,flineno,libcount,linetoread,slineno,symbol,
#ifdef	MC6809
	fivebit,indirect,
#endif
	linetoskip,optimize,pos,postf,tsym,blankline;
static char *srcfile,*lstfile,*symfile,*objfile,*srcname,list,defextend,
#ifndef	MC6809
	mc6801,
#endif
	fatalerror,quiet,onepass,undef,verbos;
static FILE *lstf,*symf;
static char linebuf[MAXCHAR];
static LIBSTACK *libstk;
static INCDIR *incdir, *incdirtail;
#ifdef	CLEANUP
static char lstarg,symarg,objarg;
#endif
#ifdef	SYSINCDIR
static char nosysinc;
#endif
/* obj.c */
static int chksum,objnext;
static char objbuf[OBJSIZE];
#if	WITH_OS9
static unsigned char os9_u[3];
static char os9_crc;
#endif
/* global */
#ifdef	DEBUGOPT
static char debug;
#endif
#if	RAW_FORMAT || IHEX_FORMAT || DECB_FORMAT
static char format;
#endif
static char dlcvalid,lcvalid,dataop,sizeinvalid,noundef,inp1,object;
static int totpass,lndigit,lndigitoff;
static int llineno,lc,llc,objlc,objbias,labels,pass,
	valid,endfile,startadr,byte,word,objpos,totsize,lastsize,
	srclineno,dlc,dllc;
static int experr,err,expdepth,sfp,nchange;
#ifdef	MC6809
static int dp,optbrcnt;
#endif
static char *parentlabel,*reflbl,nodp,globalize,setlabel;
static char *fname,*lineptr,*linebeg;
static OPTABLE *oprptr;
static LBLTABLE *label,*lblptr;
static SRCTABLE *srctab,*cursrc;
static FILE *srcf,*objf;
/*static int offset[3][4];*/
static char *namespc;
TREE *(*findsymbol)(const char *name, TREE **tree, TREE ***free);

/* prototype */
#ifdef	CLEANUP
static void cleanup(), freenode(LBLTABLE *node);
#endif
static FILE *fileopen(char *filename, const char *mode);
static LBLTABLE *createlabel(char *name, int level, LBLTABLE **lpp),
	*deflabel(), *reflabel(int what);
static char *as_strdup(const char *str), *as_strndup(const char *str, int len),
	*as_strconcat2(const char *str1, const char *str2),
	*as_strconcat3(const char *str1, const char *str2, const char *str3),
	*as_strconcat5(const char *str1, const char *str2, const char *str3,
		const char *str4, const char *str5),
	*getlabel(), *getnewlabel(unsigned char *lvl), *lineget(),
	*make_uint(char *buffer, unsigned int num);

static unsigned char hash(char *s);
static int byterange(int b), checkchar(char c), (*expression)(), expression0(),
#ifdef	MC6809
	checkbyte(int b), checkfivebit(int b), checkreg(char r),
	getreg(int r), regno(),
#endif
	expression1(), putb(int b), putw1(int w),
	pushfile(FILE *fp, char *filename, char *namspc,
		int lineno, int numline, int global),
	hexdigit(int x), isalnum(int c), isalpha(int c), iscomment(int c),
	isdigit(int c), isseparator(int c), isspace(int c), isxdigit(int c),
	isalphanumeric(int c), toupper(int c), alphanumtoint(int c),
	as_strcmpi(const char *str1, const char *str2),
#if	WITH_SIZEOF || WITH_DEFINED
	as_strncmpi(const char *str1, const char *str2, int len),
#endif
	checkgarbage(void);

static void addincdir(const char *p), assemble(), calclndigit(), clearaddress(),
	skipnonseparator(), popfile(),
#ifdef	MC6809
	indexed(int frame, int reg), postbyte(int b), printoptmark(), regop(),
#endif
	defsymbol(const char *str), dumpsymbol(),
	error(const char *msg, ...), earlyinit(), printdigit(int d, int c),
	fatal(const char *msg, const char *str, int code, int lineno),
	fileclose(FILE *f, char *filename), flushline(),
	flushobj(), initialize(), initlabel(), initline(), initpass(),
	operand(int grp, int mode), operation(), printaddress(int a),
	printbyte(int b, int c), printdecimal(unsigned int n, char *p),
	printlog(), printlogpass(FILE* f), printnode(LBLTABLE *lp),
	printtitle(int notice), printword(int w, int c), put1byte(int b),
	put1word(int w), put2hex(int b), put4hex(int w), putbyte(int b),
	putcode(int grp, int mode), putline(), putword(int w),
	refop(), skipspace(), srcnameset(), terminate(), termobj(),
	printusage(char *name), *as_malloc(unsigned int len),
	as_free(void *mem), *as_calloc(unsigned int nmemb, unsigned int size),
	toupperstr(char *str), valueerror(void),
	initoptions(void), argcheck(const char *arg);
/* srctab.c */
static void src_set(const char *filename);
static void src_resetinst(SRCTABLE *st);
#ifdef	CLEANUP
static void src_free(SRCTABLE *st);
#endif

#if defined(COMPAT_BIN)
# if defined(__GLIBC__) && defined(__x86_64__)
__asm__(".symver memcpy,memcpy@GLIBC_2.2.5");
# endif
#endif

/* EOF */
