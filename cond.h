/*
 * Copyright (C) 2019-2021 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  cond.h --- conditional
 *
 */


/* define */
#define	COND_NONE	0x00
#define	COND_BIT_VALUE	0x01
#define	COND_BIT_IF	0x02
#define	COND_BIT_ELSE	0x04
#define	COND_BIT_FORCE	0x08
#define	COND_TRUE	COND_NONE
#define	COND_FALSE	(COND_NONE^COND_BIT_VALUE)
#define	COND_ELSE	COND_BIT_ELSE
#define	COND_CC_MASK	(COND_BIT_IF|0xF0)
#define	COND_IFEQ	(COND_BIT_IF|0x10)
#define	COND_IFGE	(COND_BIT_IF|0x20)
#define	COND_IFGT	(COND_BIT_IF|0x30)
#define	COND_IFLE	(COND_BIT_IF|0x40)
#define	COND_IFLT	(COND_BIT_IF|0x50)
#define	COND_IFNE	(COND_BIT_IF|0x60)
#define	COND_IFDEF	(COND_BIT_IF|0x70)
#define	COND_IFNDEF	(COND_BIT_IF|0x80)
#define	COND_IFNREF	(COND_BIT_IF|0x90)
#define	COND_IFP1	(COND_BIT_IF|0xA0)
#define	COND_IFP2	(COND_BIT_IF|0xB0)
#define	COND_IFREF	(COND_BIT_IF|0xC0)
#define	COND_IFC	(COND_BIT_IF|0xD0)
#define	COND_IFNC	(COND_BIT_IF|0xE0)
#define	COND_ELIFEQ	(COND_BIT_ELSE|COND_IFEQ)
#define	COND_ELIFGE	(COND_BIT_ELSE|COND_IFGE)
#define	COND_ELIFGT	(COND_BIT_ELSE|COND_IFGT)
#define	COND_ELIFLE	(COND_BIT_ELSE|COND_IFLE)
#define	COND_ELIFLT	(COND_BIT_ELSE|COND_IFLT)
#define	COND_ELIFNE	(COND_BIT_ELSE|COND_IFNE)
#define	COND_ELDEF	(COND_BIT_ELSE|COND_IFDEF)
#define	COND_ELNDEF	(COND_BIT_ELSE|COND_IFNDEF)
#define	COND_ELNREF	(COND_BIT_ELSE|COND_IFNREF)
#define	COND_ELREF	(COND_BIT_ELSE|COND_IFREF)
#define	COND_ELIFC	(COND_BIT_ELSE|COND_IFC)
#define	COND_ELIFNC	(COND_BIT_ELSE|COND_IFNC)
#define	COND_END	0xF0
#define	COND_VALUE(X)		((X)&COND_BIT_VALUE)
#define	COND_IS_TRUE(X)		(((X)&COND_BIT_VALUE)==COND_TRUE)
#define	COND_IS_FALSE(X)	(((X)&COND_BIT_VALUE)==COND_FALSE)
#define	COND_TYPE(X)		((X)&(COND_BIT_IF|COND_BIT_ELSE))


/* structure */
typedef struct cs {
	struct cs *prev;
	unsigned char cond;
} CONDSTACK;


/* variable */
static unsigned char cond;
static CONDSTACK *condstk;


/* prototype */
static void condop(void), cond_free(int quiet);

