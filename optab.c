/*
 * Copyright (C) 1981, 1987 Masataka Ohta, Hiroshi Tezuka
 * Copyright (C) 2016-2021 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  optab.c --- opcode table
 *
 */

#ifndef	MC6809
#define	YES MC6801,
#define	NO 0,
#else
#define	YES
#define	NO
#endif
#define	GB (GLOBAL_FLAG_FREE | GLOBAL_TYPE_GLOBAL)
#define	XD (GLOBAL_FLAG_FREE | GLOBAL_TYPE_XDEF)
#define	XR (GLOBAL_FLAG_FREE | GLOBAL_TYPE_XREF)

	/* MNE    PREFIX  BASE       MC6801  CLASS          */
static
OPTABLE optab[] = {
#if	WITH_EXTRA
	{ "=",      DIR,  0,           NO      equ      , NULL },
#endif

#ifndef	MC6809
	{ "ABA",    0,    0x1b,        NO      none     , NULL },
#endif
	{ "ABX",    0,    0x3a,        YES     none     , NULL },
	{ "ADCA",   0,    0x89,        NO      load     , NULL },
	{ "ADCB",   0,    0xc9,        NO      load     , NULL },
	{ "ADDA",   0,    0x8b,        NO      load     , NULL },
	{ "ADDB",   0,    0xcb,        NO      load     , NULL },
	{ "ADDD",   0,    0xc3,        YES     load2    , NULL },
	{ "ANDA",   0,    0x84,        NO      load     , NULL },
	{ "ANDB",   0,    0xc4,        NO      load     , NULL },
#ifdef	MC6809
	{ "ANDCC",  0,    0x1c,        NO      ccr      , NULL },
#endif
	{ "ASL",    0,    0x08,        NO      memory   , NULL },
	{ "ASLA",   0,    0x48,        NO      none     , NULL },
	{ "ASLB",   0,    0x58,        NO      none     , NULL },
#ifndef	MC6809
	{ "ASLD",   0,    0x05,        YES     none     , NULL },
#endif
	{ "ASR",    0,    0x07,        NO      memory   , NULL },
	{ "ASRA",   0,    0x47,        NO      none     , NULL },
	{ "ASRB",   0,    0x57,        NO      none     , NULL },

	{ "BCC",    0,    0x24,        NO      branch   , NULL },
	{ "BCS",    0,    0x25,        NO      branch   , NULL },
	{ "BEQ",    0,    0x27,        NO      branch   , NULL },
	{ "BGE",    0,    0x2c,        NO      branch   , NULL },
	{ "BGT",    0,    0x2e,        NO      branch   , NULL },
	{ "BHI",    0,    0x22,        NO      branch   , NULL },
	{ "BHS",    0,    0x24,        YES     branch   , NULL },
	{ "BIN",    DATA, 0,           NO      binary   , NULL },
	{ "BITA",   0,    0x85,        NO      load     , NULL },
	{ "BITB",   0,    0xc5,        NO      load     , NULL },
	{ "BLE",    0,    0x2f,        NO      branch   , NULL },
	{ "BLO",    0,    0x25,        YES     branch   , NULL },
	{ "BLS",    0,    0x23,        NO      branch   , NULL },
	{ "BLT",    0,    0x2d,        NO      branch   , NULL },
	{ "BMI",    0,    0x2b,        NO      branch   , NULL },
	{ "BNE",    0,    0x26,        NO      branch   , NULL },
	{ "BPL",    0,    0x2a,        NO      branch   , NULL },
	{ "BRA",    0,    0x20,        NO      branch   , NULL },
	{ "BRN",    0,    0x21,        YES     branch   , NULL },
	{ "BSR",    0,    0x8d,        NO      branch   , NULL },
	{ "BVC",    0,    0x28,        NO      branch   , NULL },
	{ "BVS",    0,    0x29,        NO      branch   , NULL },

#ifndef	MC6809
	{ "CBA",    0,    0x11,        NO      none     , NULL },
	{ "CLC",    0,    0x0c,        NO      none     , NULL },
	{ "CLI",    0,    0x0e,        NO      none     , NULL },
#endif
	{ "CLR",    0,    0x0f,        NO      memory   , NULL },
	{ "CLRA",   0,    0x4f,        NO      none     , NULL },
	{ "CLRB",   0,    0x5f,        NO      none     , NULL },
#ifndef	MC6809
	{ "CLV",    0,    0x0a,        NO      none     , NULL },
#endif
	{ "CMPA",   0,    0x81,        NO      load     , NULL },
	{ "CMPB",   0,    0xc1,        NO      load     , NULL },
#ifdef	MC6809
	{ "CMPD",   0x10, 0x83,        NO      load2    , NULL },
	{ "CMPS",   0x11, 0x8c,        NO      load2    , NULL },
	{ "CMPU",   0x11, 0x83,        NO      load2    , NULL },
	{ "CMPX",   0,    0x8c,        NO      load2    , NULL },
	{ "CMPY",   0x10, 0x8c,        NO      load2    , NULL },
#endif
	{ "COM",    0,    0x03,        NO      memory   , NULL },
	{ "COMA",   0,    0x43,        NO      none     , NULL },
	{ "COMB",   0,    0x53,        NO      none     , NULL },
#ifndef	MC6809
	{ "CPX",    0,    0x8c,        NO      load2    , NULL },
#endif
#ifdef	MC6809
	{ "CWAI",   0,    0x3c,        NO      ccr      , NULL },
#endif

	{ "DAA",    0,    0x19,        NO      none     , NULL },
	{ "DEC",    0,    0x0a,        NO      memory   , NULL },
	{ "DECA",   0,    0x4a,        NO      none     , NULL },
	{ "DECB",   0,    0x5a,        NO      none     , NULL },
#ifndef	MC6809
	{ "DES",    0,    0x34,        NO      none     , NULL },
	{ "DEX",    0,    0x09,        NO      none     , NULL },
#endif
#if	WITH_TIME
	{ "DTB",    DATA, 0,           NO      dtbop    , NULL },
	{ "DTS",    DATA, 0,           NO      dtsop    , NULL },
#endif

	{ "ELDEF",  COND, COND_ELDEF,  NO      condop   , NULL },
	{ "ELIFC",  COND, COND_ELIFC,  NO      condop   , NULL },
	{ "ELIFEQ", COND, COND_ELIFEQ, NO      condop   , NULL },
	{ "ELIFGE", COND, COND_ELIFGE, NO      condop   , NULL },
	{ "ELIFGT", COND, COND_ELIFGT, NO      condop   , NULL },
	{ "ELIFLE", COND, COND_ELIFLE, NO      condop   , NULL },
	{ "ELIFLT", COND, COND_ELIFLT, NO      condop   , NULL },
	{ "ELIFNC", COND, COND_ELIFNC, NO      condop   , NULL },
	{ "ELIFNE", COND, COND_ELIFNE, NO      condop   , NULL },
	{ "ELNDEF", COND, COND_ELNDEF, NO      condop   , NULL },
	{ "ELNREF", COND, COND_ELNREF, NO      condop   , NULL },
	{ "ELREF",  COND, COND_ELREF,  NO      condop   , NULL },
	{ "ELSE",   COND, COND_ELSE,   NO      condop   , NULL },
	{ "END",    DIR,  0,           NO      endop    , NULL },
	{ "ENDC",   COND, COND_END,    NO      condop   , NULL },
#if	WITH_IFENDIF
	{ "ENDIF",  COND, COND_END,    NO      condop   , NULL },
#endif
	{ "ENDM",   MACR, 0,           NO      endmop   , NULL },
#if	WITH_STRUCT
	{ "ENDS",   ENDS, 0,           NO      endsop   , NULL },
#endif
#if	WITH_OS9
	{ "EMOD",   DATA, 0,           NO      emodop   , NULL },
#endif
	{ "EORA",   0,    0x88,        NO      load     , NULL },
	{ "EORB",   0,    0xc8,        NO      load     , NULL },
	{ "EQU",    DIR,  0,           NO      equ      , NULL },
	{ "ERR",    OKSD, 0,           NO      errop    , NULL },
	{ "EXIT",   DIR,  0,           NO      exitop   , NULL },
#ifdef	MC6809
	{ "EXG",    0,    0x1e,        NO      transfer , NULL },
#endif

	{ "FCB",    DATA, 0,           NO      fcb      , NULL },
	{ "FCC",    DATA, 0,           NO      fcc      , NULL },
	{ "FCN",    DATA, 1,           NO      fcc      , NULL },
	{ "FCS",    DATA, 0,           NO      fcs      , NULL },
	{ "FDB",    DATA, 1,           NO      fcb      , NULL },
#if	WITH_EXTRA
	{ "FDBS",   DATA, 2,           NO      fcb      , NULL },
	{ "FILL",   DATA, 0,           NO      fillop   , NULL },
#endif
	{ "FLB",    DATA, 1,           NO      flzbdop  , NULL },
	{ "FLD",    DATA, 0,           NO      flzbdop  , NULL },
#if	WITH_EXTRA
	{ "FZB",    DATA, 255,         NO      flzbdop  , NULL },
#endif

	{ "GLOBL",  DIR,  GB,          NO      globlop  , NULL },

#if	WITH_IFENDIF
	{ "IF",     COND, COND_IFNE,   NO      condop   , NULL },
#endif
	{ "IFC",    COND, COND_IFC,    NO      condop   , NULL },
	{ "IFDEF",  COND, COND_IFDEF,  NO      condop   , NULL },
	{ "IFEQ",   COND, COND_IFEQ,   NO      condop   , NULL },
	{ "IFGE",   COND, COND_IFGE,   NO      condop   , NULL },
	{ "IFGT",   COND, COND_IFGT,   NO      condop   , NULL },
	{ "IFLE",   COND, COND_IFLE,   NO      condop   , NULL },
	{ "IFLT",   COND, COND_IFLT,   NO      condop   , NULL },
	{ "IFNC",   COND, COND_IFNC,   NO      condop   , NULL },
	{ "IFNDEF", COND, COND_IFNDEF, NO      condop   , NULL },
	{ "IFNE",   COND, COND_IFNE,   NO      condop   , NULL },
	{ "IFNREF", COND, COND_IFNREF, NO      condop   , NULL },
	{ "IFP1",   COND, COND_IFP1,   NO      condop   , NULL },
	{ "IFP2",   COND, COND_IFP2,   NO      condop   , NULL },
	{ "IFREF",  COND, COND_IFREF,  NO      condop   , NULL },
	{ "INC",    0,    0x0c,        NO      memory   , NULL },
	{ "INCA",   0,    0x4c,        NO      none     , NULL },
	{ "INCB",   0,    0x5c,        NO      none     , NULL },
#ifndef	MC6809
	{ "INS",    0,    0x31,        NO      none     , NULL },
	{ "INX",    0,    0x08,        NO      none     , NULL },
#endif

	{ "JMP",    0,    0x0e,        NO      memory   , NULL },
	{ "JSR",    0,    0x8d,        NO      store    , NULL },

#ifdef	MC6809
	{ "LBCC",   0x10, 0x24,        NO      lbranch  , NULL },
	{ "LBCS",   0x10, 0x25,        NO      lbranch  , NULL },
	{ "LBEQ",   0x10, 0x27,        NO      lbranch  , NULL },
	{ "LBGE",   0x10, 0x2c,        NO      lbranch  , NULL },
	{ "LBGT",   0x10, 0x2e,        NO      lbranch  , NULL },
	{ "LBHI",   0x10, 0x22,        NO      lbranch  , NULL },
	{ "LBHS",   0x10, 0x24,        NO      lbranch  , NULL },
	{ "LBLE",   0x10, 0x2f,        NO      lbranch  , NULL },
	{ "LBLO",   0x10, 0x25,        NO      lbranch  , NULL },
	{ "LBLS",   0x10, 0x23,        NO      lbranch  , NULL },
	{ "LBLT",   0x10, 0x2d,        NO      lbranch  , NULL },
	{ "LBMI",   0x10, 0x2b,        NO      lbranch  , NULL },
	{ "LBNE",   0x10, 0x26,        NO      lbranch  , NULL },
	{ "LBPL",   0x10, 0x2a,        NO      lbranch  , NULL },
	{ "LBRA",   0,    0x16,        NO      lbranch  , NULL },
	{ "LBRN",   0x10, 0x21,        NO      lbranch  , NULL },
	{ "LBSR",   0,    0x17,        NO      lbranch  , NULL },
	{ "LBVC",   0x10, 0x28,        NO      lbranch  , NULL },
	{ "LBVS",   0x10, 0x29,        NO      lbranch  , NULL },
#endif
#ifdef	MC6809
	{ "LDA",    0,    0x86,        NO      load     , NULL },
	{ "LDB",    0,    0xc6,        NO      load     , NULL },
#else
	{ "LDAA",   0,    0x86,        NO      load     , NULL },
	{ "LDAB",   0,    0xc6,        NO      load     , NULL },
#endif
	{ "LDD",    0,    0xcc,	       YES     load2    , NULL },
#ifdef	MC6809
	{ "LDS",    0x10, 0xce,        NO      load2    , NULL },
	{ "LDU",    0,    0xce,        NO      load2    , NULL },
	{ "LDX",    0,    0x8e,        NO      load2    , NULL },
	{ "LDY",    0x10, 0x8e,        NO      load2    , NULL },
	{ "LEAS",   0,    0x32,        NO      lea      , NULL },
	{ "LEAU",   0,    0x33,        NO      lea      , NULL },
	{ "LEAX",   0,    0x30,        NO      lea      , NULL },
	{ "LEAY",   0,    0x31,        NO      lea      , NULL },
#else
	{ "LDS",    0,    0x8e,        NO      load2    , NULL },
	{ "LDX",    0,    0xce,        NO      load2    , NULL },
#endif
	{ "LIB",    DIR,  0,           NO      library  , NULL },
	{ "LSL",    0,    0x08,        NO      memory   , NULL },
	{ "LSLA",   0,    0x48,        NO      none     , NULL },
	{ "LSLB",   0,    0x58,        NO      none     , NULL },
#ifndef	MC6809
	{ "LSLD",   0,    0x05,        YES     none     , NULL },
#endif
	{ "LSR",    0,    0x04,        NO      memory   , NULL },
	{ "LSRA",   0,    0x44,        NO      none     , NULL },
	{ "LSRB",   0,    0x54,        NO      none     , NULL },
#ifndef	MC6809
	{ "LSRD",   0,    0x04,        YES     none     , NULL },
#endif

	{ "MACR",   MACR, 0,           NO      macroop  , NULL },
#if	WITH_OS9
	{ "MOD",    DATA, 0,           NO      modop    , NULL },
#endif
	{ "MUL",    0,    0x3d,        YES     none     , NULL },

	{ "NAM",    DIR,  0,           NO      nullop   , NULL },
	{ "NAME",   DIR,  0,           NO      nullop   , NULL },
	{ "NEG",    0,    0x00,        NO      memory   , NULL },
	{ "NEGA",   0,    0x40,        NO      none     , NULL },
	{ "NEGB",   0,    0x50,        NO      none     , NULL },
	{ "NIL",    OKSD, 0,           NO      nil      , NULL },
	{ "NODP",   DIR,  0,           NO      nodpop   , NULL },
#ifdef	MC6809
	{ "NOP",    0,    0x12,        NO      none     , NULL },
#else
	{ "NOP",    0,    0x01,        NO      none     , NULL },
#endif

	{ "OPT",    DIR,  0,           NO      optop    , NULL },
#ifdef	MC6809
	{ "ORA",    0,    0x8a,        NO      load     , NULL },
	{ "ORB",    0,    0xca,        NO      load     , NULL },
	{ "ORCC",   0,    0x1a,        NO      ccr      , NULL },
#else
	{ "ORAA",   0,    0x8a,        NO      load     , NULL },
	{ "ORAB",   0,    0xca,        NO      load     , NULL },
#endif
	{ "ORG",    DATA, 0,           NO      org      , NULL },
#if	WITH_OS9
	{ "OS9",    0x10, 0x3f,        NO      os9op    , NULL },
#endif

	{ "PAG",    DIR,  0,           NO      nullop   , NULL },
	{ "PAGE",   DIR,  0,           NO      nullop   , NULL },
#ifdef	MC6809
	{ "PSHS",   0,    0x34,        NO      stacks   , NULL },
	{ "PSHU",   0,    0x36,        NO      stacku   , NULL },
	{ "PULS",   0,    0x35,        NO      stacks   , NULL },
	{ "PULU",   0,    0x37,        NO      stacku   , NULL },
#else
	{ "PSHA",   0,    0x36,        NO      none     , NULL },
	{ "PSHB",   0,    0x37,        NO      none     , NULL },
	{ "PSHX",   0,    0x3c,        YES     none     , NULL },
	{ "PULA",   0,    0x32,        NO      none     , NULL },
	{ "PULB",   0,    0x33,        NO      none     , NULL },
	{ "PULX",   0,    0x38,        YES     none     , NULL },
#endif

	{ "REF",    DIR,  0,           NO      refop    , NULL },
#ifdef	MC6809
	{ "REG",    DIR,  0,           NO      regop    , NULL },
#endif
	{ "RMB",    RMB,  0,           NO      rmb      , NULL },
	{ "ROL",    0,    0x09,        NO      memory   , NULL },
	{ "ROLA",   0,    0x49,        NO      none     , NULL },
	{ "ROLB",   0,    0x59,        NO      none     , NULL },
	{ "ROR",    0,    0x06,        NO      memory   , NULL },
	{ "RORA",   0,    0x46,        NO      none     , NULL },
	{ "RORB",   0,    0x56,        NO      none     , NULL },
	{ "RTI",    0,    0x3b,        NO      none     , NULL },
	{ "RTS",    0,    0x39,        NO      none     , NULL },

#ifndef	MC6809
	{ "SBA",    0,    0x10,        NO      none     , NULL },
#endif
	{ "SBCA",   0,    0x82,        NO      load     , NULL },
	{ "SBCB",   0,    0xc2,        NO      load     , NULL },
#ifndef	MC6809
	{ "SEC",    0,    0x0d,        NO      none     , NULL },
	{ "SEI",    0,    0x0f,        NO      none     , NULL },
	{ "SEV",    0,    0x0b,        NO      none     , NULL },
#endif
	{ "SET",    DIR,  1,           NO      equ      , NULL },
#ifdef	MC6809
	{ "SETDP",  DIR,  0,           NO      setdp    , NULL },
	{ "SEX",    0,    0x1d,        NO      none     , NULL },
#endif
	{ "SPC",    DIR,  0,           NO      nullop   , NULL },
#ifdef	MC6809
	{ "STA",    0,    0x87,        NO      store    , NULL },
	{ "STB",    0,    0xc7,        NO      store    , NULL },
#else
	{ "STAA",   0,    0x87,        NO      store    , NULL },
	{ "STAB",   0,    0xc7,        NO      store    , NULL },
#endif
	{ "STD",    0,    0xcd,        YES     store    , NULL },
#if	WITH_STRUCT
	{ "STRUCT", STRD, 0,           NO      structop , NULL },
#endif
#ifdef	MC6809
	{ "STS",    0x10, 0xcf,        NO      store    , NULL },
#endif
	{ "STTL",   DIR,  0,           NO      nullop   , NULL },
#ifdef	MC6809
	{ "STU",    0,    0xcf,        NO      store    , NULL },
	{ "STX",    0,    0x8f,        NO      store    , NULL },
	{ "STY",    0x10, 0x8f,        NO      store    , NULL },
#else
	{ "STS",    0,    0x8f,        NO      store    , NULL },
	{ "STX",    0,    0xcf,        NO      store    , NULL },
#endif
	{ "SUBA",   0,    0x80,        NO      load     , NULL },
	{ "SUBB",   0,    0xc0,        NO      load     , NULL },
	{ "SUBD",   0,    0x83,        YES     load2    , NULL },
	{ "SWI",    0,    0x3f,        NO      none     , NULL },
#ifdef	MC6809
	{ "SWI2",   0x10, 0x3f,        NO      none     , NULL },
	{ "SWI3",   0x11, 0x3f,        NO      none     , NULL },
	{ "SYNC",   0,    0x13,        NO      none     , NULL },
#endif

#ifdef	MC6809
	{ "TFR",    0,    0x1f,        NO      transfer , NULL },
#else
	{ "TAB",    0,    0x16,        NO      none     , NULL },
	{ "TAP",    0,    0x06,        NO      none     , NULL },
	{ "TBA",    0,    0x17,        NO      none     , NULL },
	{ "TPA",    0,    0x07,        NO      none     , NULL },
#endif
	{ "TST",    0,    0x0d,        NO      memory   , NULL },
	{ "TSTA",   0,    0x4d,        NO      none     , NULL },
	{ "TSTB",   0,    0x5d,        NO      none     , NULL },
#ifndef	MC6809
	{ "TSX",    0,    0x30,        NO      none     , NULL },
#endif
	{ "TTL",    DIR,  0,           NO      nullop   , NULL },
#ifndef	MC6809
	{ "TXS",    0,    0x35,        NO      none     , NULL },
#endif

#if	WITH_EXTRA
	{ "USE",    DIR,  1,           NO      library  , NULL },
#endif

#ifndef	MC6809
	{ "WAI",    0,    0x3e,        NO      none     , NULL },
#endif
	{ "WITH",   DIR,  0,           NO      withop   , NULL },

	{ "XDEF",   DIR,  XD,          NO      globlop  , NULL },
	{ "XREF",   DIR,  XR,          NO      globlop  , NULL },

	{ "ZMB",    DATA, 255,         NO      flzbdop  , NULL },

	{ "",       0,    0,           NO      none     , NULL }
};

#undef	YES
#undef	NO
#undef	GB
#undef	XD
#undef	XR

/* opcode table hash */
static
OPTABLE *ophash[256];

/* addressing mode offset table 'offset[group][mode]' */
static
int	offset[3][4] = {
	{ 0,    0,    0,    0    },
	{ 0,    0,    0x60, 0x70 },
	{ 0,    0x10, 0x20, 0x30 }
};

/* hash function */
static
unsigned char hash(char *s)
{
#ifdef	MC6809 /* 137/161 */
/*#define HASHSEED 40
#define HASHMULT 229*/
#define HASHSEED 36 /* 148/162 */
#define HASHMULT 153
	static unsigned char salt[MNEMOSIZE] = {0x56, 0xEA, 0xC7, 0x48, 0xF7};
#else	/*!MC6809 127/145 */
/*#define HASHSEED 55
#define HASHMULT 163*/
#define HASHSEED 24 /* 135/146 */
#define HASHMULT 105
	static unsigned char salt[MNEMOSIZE] = {0xA5, 0x17, 0xCA, 0x7B, 0x98};
#endif
	unsigned char h = HASHSEED, *r = salt;
	while ( *s ) h = h * (unsigned char)HASHMULT ^ (unsigned char)*s++ ^ *r++;
/*	while ( *s ) h = (~h * (unsigned char)HASHMULT) + (unsigned char)*s++;*/
	return h;
#undef HASHSEED
#undef HASHMULT
}
