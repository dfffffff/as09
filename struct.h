/*
 * Copyright (C) 2021 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  struct.h --- structure handling
 *
 */


/* structure */

typedef struct sl {
	struct sl *next;
	char *line;
	unsigned char prefix;
} STRUCTLINE;

typedef struct stbl {
	TREE t;
	STRUCTLINE *lines, *tail;
	SRCTABLE *srcfile;
	int srclineno;
	char expand;
} STRUCTTABLE;

typedef struct ss {
	struct ss *prev;
	STRUCTTABLE *structure;
	STRUCTLINE *line;
	STRUCTLINE **linep;
	char *name;
	char *last;
	char start;
	char end;
} STRUCTSTACK;


/* variable */
static unsigned int struct_defs;
static char struct_from_def;
static unsigned char struct_level;
static STRUCTSTACK *structstk;


/* prototype */
static void endsop(void), structop(void),
	struct_addline(const char *line, unsigned char prefix),
	struct_init(void), struct_enter(STRUCTTABLE *st, int def),
	struct_popfile(void);
static int struct_getline(char *buffer, int size);
static STRUCTTABLE *struct_check(const char *name);
#ifdef	CLEANUP
static void struct_cleanup(void);
#endif


