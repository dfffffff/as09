/*
 * Copyright (C) 1981, 1987 Masataka Ohta, Hiroshi Tezuka
 * Copyright (C) 2016-2021 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  oppro.c --- opcode processing
 *
 */


static
void optop()
{	skipspace();
	lineptr = parseoption(lineptr, 0);
}

static
void withop()
{	char *label;
	skipspace();
	label = getlabel();
	if ( *label ) {
		if ( strchr(label, '.') )
			error("extra dot in label");
		else {
			as_free(parentlabel);
			parentlabel = label;
			label = NULL;
		}
	}
	as_free(label);
}

static
void nullop()
{
}

static
void none()
{	putcode(GROUP0,NO_MODE);
}

static
void load()
{	operand(GROUP2,LOAD);
}

static
void load2()
{	operand(GROUP2,LOAD2);
}

static
void memory()
{	operand(GROUP1,MEMORY);
}

static
void store()
{	operand(GROUP2,STORE);
}

static
void branch()
{	int val;
	skipspace();
	val = expression() - llc - 2;
	if ( word )
		error("wrong operand size for short branch");
	if ( !byterange(val) )
		error("short branch too far");
	putcode(GROUP0,NO_MODE);
	putbyte(val);
}

#ifdef	MC6809
static
void lbranch()
{	int val;
	OPTABLE shortop;
	skipspace();
	val = expression() - llc - 2;
	if ( byte )
		error("wrong operand size for long branch");
	if ( optlongbranch && valid && !word && byterange(val) )
	{	optbrcnt++;
		printoptmark();
		shortop.prefix = 0;
		if ( oprptr->opcode == 0x16 )			/* LBRA */
			shortop.opcode = 0x20;			/* BRA  */
		else if ( oprptr->opcode == 0x17 )		/* LBSR */
			shortop.opcode = 0x8d;			/* BSR  */
		else
			shortop.opcode = oprptr->opcode;	/* conditional */
		oprptr = &shortop;
		putcode(GROUP0,NO_MODE);
		putbyte(val);
		return;
	}
	putcode(GROUP0,NO_MODE);
	putword(val - 1 - !!oprptr->prefix);
}

static
void lea()
{	operand(GROUP0,INDEX);
}

static
void transfer()
{	int r1,r2;
	skipspace();
	putcode(GROUP0,NO_MODE);
	if ( (r1 = regno()) < 0 ) error("register required");
	if ( !checkchar(',') ) error("missing ','");
	if ( (r2 = regno()) < 0 ) error("register required");
	if ( (r1 ^ r2) & 0x08 ) error("invalid register combination");
	putbyte((r1 << 4) | r2);
}

static
void ccr()
{	operand(GROUP0,IMMEDIATE);
}

static
void pushpul(int u)
{	int m1=0, m2=0;
	skipspace();
	if ( u >= 0 && checkchar('#') )
	{	m1 = expression();
		if ( m1 & ~0xFF )
			error("invalid register");
	}
	else if ( *lineptr != EOL )
		do
		{	switch ( getreg(ALLREG) )
			{	case CC: m2 = 0x01; break;
				case A:  m2 = 0x02; break;
				case B:  m2 = 0x04; break;
				case D:  m2 = 0x06; break;
				case DP: m2 = 0x08; break;
				case X:  m2 = 0x10; break;
				case Y:  m2 = 0x20; break;
				case S: if ( u == 0 ) error("invalid register"); m2 = 0x40; break;
				case U: if ( u >  0 ) error("invalid register"); m2 = 0x40; break;
				case PC: m2 = 0x80;
			} if ( m1 & m2 ) error("same register");
			m1 |= m2;
		} while ( checkchar(',') );
	if ( u >= 0 )
	{	putcode(GROUP0,NO_MODE);
		putbyte(m1);
	}
	else
	{	if ( lblptr )
		{	if ( lblptr->type!=SYM_EQUATE || lblptr->valid!=1 || lblptr->value!=m1 )
				nchange++;
			lblptr->type = SYM_EQUATE;
			lblptr->valid = 1;
			lblptr->value = m1;
			lblptr->pass = totpass;
			lblptr->inp1 = inp1;
		}
		else
			error("%s without label", oprptr->mnemonic);
		printbyte(m1,LOD);
		lblptr = NULL;
	}
}

static
void stacks()
{	pushpul(0);
}

static
void stacku()
{	pushpul(1);
}

static
void regop()
{	pushpul(-1);
}

#endif

static
void nodpop()
{	char newnodp;
	skipspace();
	noundef = 1;
	newnodp = !!expression();
	if ( valid )
		printbyte(nodp = newnodp,LOD);
}

#ifdef	MC6809
static
void setdp()
{
	int newdp;
	skipspace();
	noundef = 1;
	newdp = expression();
	if ( valid )
	{	if ( (unsigned)newdp > 255 )
			error("SETDP value overflow, taking low byte");
		printbyte(dp = (newdp&255),LOD);
	}
}
#endif

static
#if	WITH_EXTRA
int processfname(char **newfname, char **curpath, int useop)
#else
int processfname(char **newfname, char **curpath)
#endif
{	int len;
	char type, *tmpfname, *str, endchar;
#if	WITH_EXTRA
	type = 0;
#endif
	if ( checkchar('\"') ) {
		type = 1;
		endchar = '\"'; }
	else if ( checkchar('<') ) {
		type = 2;
		endchar = '>'; }
	else
#if	WITH_EXTRA
	if ( !useop )
#endif
	{
		error("expects a file name");
		return 0; }
#if	WITH_EXTRA
	if ( type ) {
#endif
	for ( len = 0, str = lineptr; *lineptr != EOL && *lineptr != endchar ; lineptr++, len++ ) {}
	if ( *lineptr != endchar ) {
		error("missing delimiter");
		return 0; }
	lineptr++;
#if	WITH_EXTRA
	}
	if ( useop ) {
		if ( !type )
			for ( len = 0, str = lineptr; *lineptr != EOL && !isseparator(*lineptr) ; lineptr++, len++ ) {}
		type = 2;
	}
#endif
	*newfname = as_strndup(str, len);
	if ( (str=strrchr(fname, PATHSEPCHR)) )
		*curpath = as_strndup(fname, str-fname+1);
	else
		*curpath = as_strdup("");
	if ( type == 1 && **newfname != PATHSEPCHR ) {
		tmpfname = as_strconcat2(*curpath, *newfname);
		as_free(*newfname);
		*newfname = tmpfname;
	}
	return type;
}

static
int searchfopen(const char *newfname, const char *curpath, char **fname, FILE **fp, const char *mode)
{
	INCDIR *id;
	char *filename;
	FILE *f;
	for (id=incdir; id; id=id->next) {
		if ( *id->path == PATHSEPCHR ) {
			/* try absolute path */
			filename = as_strconcat3(id->path, PATHSEPSTR, newfname);
			if ( (f = fopen(filename,mode)) != NULL )
				goto found;
			as_free(filename);
		}
		else {
			/* try relative to the current source file */
			filename = as_strconcat5(curpath, id->path, PATHSEPSTR, newfname, "");
			if ( (f = fopen(filename,mode)) != NULL )
				goto found;
			as_free(filename);
			if ( optcwd ) {
				/* try relative to the current working directory */
				filename = as_strconcat3(id->path, PATHSEPSTR, newfname);
				if ( (f = fopen(filename,mode)) != NULL )
					goto found;
				as_free(filename);
			}
		}
	}
	return 0;
found:
	*fname = filename;
	*fp = f;
	return 1;
}

static
void library()
{	int offset, size, type;
	char *newfname, *curpath, *fname, *namespc, global;
	FILE *fp;
	skipspace();
#if	WITH_EXTRA
	type = processfname(&newfname,&curpath,oprptr->opcode);
#else
	type = processfname(&newfname,&curpath);
#endif
	if ( !type )
		return;
	if ( verbos > 1 )
		fprintf(stderr,"%s: %s\n",oprptr->mnemonic,newfname);
	fname = NULL;
	namespc = NULL;
	offset = 0;
	size = -1;
	global = 0;
	if ( checkchar(',') ) { /* skipped lines */
		noundef = 1;
		if ( *lineptr != ',' ) {
			offset = expression();
			if ( experr || !valid ) {
				sizeinvalid = 1;
				goto exit; } }
		if ( checkchar(',') ) { /* max number of lines read */
			if ( *lineptr != ',' ) {
				size = expression();
				if ( experr || !valid ) {
					sizeinvalid = 1;
					goto exit; } }
			if ( checkchar(',') ) { /* namespace */
				global = checkchar('*');
				if ( !global ) {
					global = 2; /* will turn off globalize */
					if ( !isseparator(*lineptr) && !*(namespc=getlabel()) )
						goto exit; } } } }
	if ( checkgarbage() )
		goto exit;
	if ( type == 1 || !incdir || *newfname == PATHSEPCHR ) {
		if ( (fp = fopen(newfname,"r")) == NULL ) {
#ifdef	CLEANUP
			fatal("can't open library file \"%s\"",newfname,0,1);
			goto faterr;
#else
			fatal("can't open library file \"%s\"",newfname,FIOERROR,1);
#endif
		}
		fname = newfname;
		newfname = NULL;
	}
	else {
		if ( !searchfopen(newfname,curpath,&fname,&fp,"r") ) {
#ifdef	CLEANUP
			fatal("can't find or open library file \"%s\"",newfname,0,1);
			goto faterr;
#else
			fatal("can't find or open library file \"%s\"",newfname,FIOERROR,1);
#endif
		}
	}
	if ( !pushfile(fp,fname,namespc,offset,size,global) )
		fclose(fp);
	else {
		fname = NULL;
		namespc = NULL;
	}
exit:
	as_free(fname);
	as_free(namespc);
	as_free(curpath);
	as_free(newfname);
	return;
#ifdef	CLEANUP
faterr:
	as_free(fname);
	as_free(namespc);
	as_free(curpath);
	as_free(newfname);
	exit(FIOERROR);
#endif
}

static
void binary()
{	int offset, size, c, type;
	char *newfname, *curpath, *fname;
	FILE *fp;
	skipspace();
	fname = NULL;
#if	WITH_EXTRA
	type = processfname(&newfname,&curpath,0);
#else
	type = processfname(&newfname,&curpath);
#endif
	if ( !type )
		return;
	if ( verbos > 1 )
		fprintf(stderr,"%s: %s\n",oprptr->mnemonic,newfname);
	offset = size = -1;
	if ( checkchar(',') ) {
		noundef = 1;
		if ( *lineptr != ',' ) {
			offset = expression();
			if ( experr || !valid ) {
				sizeinvalid = 1;
				goto exit; } }
		if ( checkchar(',') ) {
			size = expression();
			if ( experr || !valid ) {
				sizeinvalid = 1;
				goto exit; } } }
	if ( checkgarbage() )
		goto exit;
	if ( type == 1 || !incdir || *newfname == PATHSEPCHR ) {
		if ( (fp = fopen(newfname,"rb")) == NULL ) {
#ifdef	CLEANUP
			fatal("can't open binary file \"%s\"",newfname,0,1);
			goto faterr;
#else
			fatal("can't open binary file \"%s\"",newfname,FIOERROR,1);
#endif
		}
		fname = newfname;
		newfname = NULL;
	}
	else {
		if ( !searchfopen(newfname,curpath,&fname,&fp,"rb") ) {
#ifdef	CLEANUP
			fatal("can't find or open binary file \"%s\"",newfname,0,1);
			goto faterr;
#else
			fatal("can't find or open binary file \"%s\"",newfname,FIOERROR,1);
#endif
		}
	}
	if ( offset >= 0 )
		if ( fseek(fp,offset,SEEK_SET) ) {
#ifdef	CLEANUP
			fatal("can't seek binary file \"%s\"",fname,0,1);
			goto faterr;
#else
			fatal("can't seek binary file \"%s\"",fname,FIOERROR,1);
#endif
		}
	while ( (c = fgetc(fp)) != -1 && (size < 0 || size--) )
		put1byte(c);
	fclose(fp);
exit:
	as_free(fname);
	as_free(curpath);
	as_free(newfname);
	return;
#ifdef	CLEANUP
faterr:
	as_free(fname);
	as_free(curpath);
	as_free(newfname);
	exit(FIOERROR);
#endif
}

static
void dataopprep(void)
{	int dlcvalue;
	if ( optdatsep ) {
		dataop = 1;
		printaddress(dlc);
		if ( lblptr ) {
			dlcvalue = dlcvalid ? dllc : 0;
			if ( lblptr->valid!=dlcvalid || lblptr->value!=dlcvalue )
				nchange++;
			lblptr->valid = dlcvalid;
			lblptr->value = dlcvalue;
			lblptr->pass = totpass;
			lblptr->man = 1;
			lblptr->inp1 = inp1;
		}
	}
}

static
void org()
{	int origin;
	dataopprep();
	skipspace();
	if ( !optorg && checkchar(',') )
		printword(objbias = expression(),LOD);
	else
	{	noundef = 1;
		origin = expression();
		noundef = 0;
		sizeinvalid = !valid ? 1 : 2;
		if ( checkchar(',') )
			printword(objbias = expression(),LOD);
		printword(*(optdatsep ? &dlc : &lc) = origin,LOO);
	}
}

static
void rmb()
{	int bytes;
	dataopprep();
	skipspace();
	noundef = 1;
	bytes = expression();
	if ( !valid )
	{	sizeinvalid = 1;
		return;
	}
	if ( bytes < 0 )
	{	error("%s value %d is negative", oprptr->mnemonic, bytes);
		return;
	}
	if ( !optdatsep )
		lc += bytes;
	else
		dlc += bytes;
	printword(bytes,LOD);
}

static
void nil()
{	int val;
	skipspace();
	val = expression();
	if ( val != 0 )
	{	error("%s value %d is not null", oprptr->mnemonic, val);
		printword(val,LOD);
	}
}

static
void errop()
{	int len, c;
	skipspace();
	len = strlen(lineptr);
	c = lineptr[len-1];
	lineptr[len-1] = 0;
	error("%s", lineptr);
	lineptr[len-1] = c;
}

static
void equ()
{	int type, val=0;
	type = oprptr->opcode ? SYM_SET : SYM_EQUATE;
	if ( type == SYM_SET )
		setlabel = 0;
	skipspace();
	if ( lblptr )
	{	if ( lblptr->type != type )
		{	lblptr->type = type;
			lblptr->value = 0;
			lblptr->valid = 0;
			nchange++;
		}
	}
	noundef = 1 + (type==SYM_SET ? optundefset : 0);
	val = expression();
	if ( lblptr )
	{	if ( (lblptr->valid!=valid && (type!=SYM_SET || !optundefset)) || (type!=SYM_SET && lblptr->value!=val) )
			nchange++;
		lblptr->valid = valid;
		lblptr->value = val;
		lblptr->pass = totpass;
		lblptr->inp1 = inp1;
	}
	else
		error("%s without label", oprptr->mnemonic);
	printword(val,LOD);
	lblptr = NULL;
}

static
void fcb()
{	int val, oldllc = llc;
	char word = oprptr->opcode;
	skipspace();
	do
	{	while ( checkchar(',') ) {}
		if ( *lineptr == EOL || isspace(*lineptr) )
			break;
		if ( checkchar('"') )
		{	while ( *lineptr != '"' && *lineptr != EOL )
				if (word)
					put1word((unsigned char)*lineptr++);
				else
					put1byte(*lineptr++);
			if ( *lineptr != '"' )
				error("missing delimiter");
			else
				lineptr++;
		}
		else
		{	if ( !optaup )
				llc = lc;
			val = expression();
			if ( word ) {
#if	WITH_EXTRA
				if ( word > 1 )
					val = ((val << 8) & 0xFF00) | ((val >> 8) & 0x00FF);
#endif
				put1word(val);
			}
			else {
				if ( val < -128 || 255 < val )
					valueerror();
				put1byte(val); }
		}
	} while ( checkchar(',') );
	llc = oldllc;
}

static
void fcc()
{	char c;
	char null = oprptr->opcode;
	skipspace();
	do
	{	while ( (c = *lineptr++) == ',' ) {}
		if ( c == EOL || isspace(c) )
		{	lineptr--;
			return;
		}
		if ( isalphanumeric(c) )
		{	error("illegal delimiter");
			return;
		}
		while ( *lineptr != c )
		{	if ( *lineptr == EOL )
			{	error("missing delimiter");
				return;
			}
			else put1byte(*lineptr++);
		}
		if ( null )
			put1byte(0);
		lineptr++;
	} while ( checkchar(',') );
}

static
void fcs()
{	char c;
	skipspace();
	do
	{	while ( (c = *lineptr++) == ',' ) {}
		if ( c == EOL || isspace(c) )
		{	lineptr--;
			return;
		}
		if ( isalphanumeric(c) )
		{	error("illegal delimiter");
			return;
		}
		while ( ((*lineptr != c) && (*lineptr != EOL) && (*(lineptr+1) != c)) )
			put1byte((*lineptr++)&0x7F);
		if ( *lineptr == EOL )
		{	error("missing delimiter");
			return;
		}
		if ( *lineptr == c )
			put1byte(0x80);
		else
			put1byte(*lineptr|0x80);
		if ( *++lineptr == c )
			lineptr++;
	} while ( checkchar(',') );
}

/* ZMB: byte=-1, FLB: byte=1, FLD: byte=0 */
static
void flzbdop()
{	int i, c=0, s=0;
	signed char byte = oprptr->opcode;
	skipspace();
	noundef = 1;
	i = expression();
	if ( !valid )
	{	sizeinvalid = 1;
		return;
	}
	if ( i < 0 )
	{	error("%s count value %d is negative", oprptr->mnemonic, i);
		return;
	}
	if ( byte >= 0 && checkchar(',') )
	{	noundef = 0;
		c = expression();
		if ( checkchar(',') )
			s = expression();
	}
	if ( byte == 0 )
		for ( ; i>0; i--,c+=s )
			put1word(c);
	else
		for ( ; i>0; i--,c+=s )
			put1byte(c);
}

static
void refop()
{	skipspace();
	do
	{	if ( !reflabel(REF_EXPRESSION) )
			UNDEFINED_SYMBOL();
	} while ( checkchar(',') );
}

static
void endop()
{	skipspace();
	if ( *lineptr != EOL )
	{	startadr = expression();
		printword(startadr,LOD);
	}
	popfile();
}

#if	WITH_OS9
static
void modop()
{
	unsigned char i, e, n, chksum;
	int param[6];
	if ( os9_crc ) {
		error("nested %s", oprptr->mnemonic);
		clearaddress();
		return;
	}
	skipspace();
	lcvalid = 1;
	llc = lc = 0;
	dlcvalid = 1;
	dllc = dlc = 0;
	sizeinvalid = 2;
	if ( lblptr ) {
		if ( lblptr->valid!=1 || lblptr->value!=0 )
			nchange++;
		lblptr->valid = 1;
		lblptr->value = 0;
		lblptr->pass = totpass;
		lblptr->man = 2;
		lblptr->inp1 = inp1;
		printaddress(0);
	}
	os9_crc = 1;
	os9_u[0] = os9_u[1] = os9_u[2] = 0xFF;
	if ( !isseparator(*lineptr) ) {
		for ( n=0, e=0, i=0; i<6; i++ ) {
			if ( !e ) {
				n++;
				param[i] = expression();
				if ( !checkchar(',') ) {
					if ( n < 4 || n == 5 )
						error("missing argument");
					e = 1;
				}
			}
			else
				param[i] = 0;
		}
		chksum = 0xFF ^ 0x87 ^ 0xCD ^
			(param[0]>>8) ^ param[0] ^
			(param[1]>>8) ^ param[1] ^
			param[2] ^ param[3];
		put1word(0x87CD);
		put1word(param[0]);
		put1word(param[1]);
		put1byte(param[2]);
		put1byte(param[3]);
		put1byte(chksum);
		if ( n >= 5 ) {
			put1word(param[4]);
			totsize += 2;
		}
		if ( n >= 6 ) {
			put1word(param[5]);
			totsize += 2;
		}
		totsize += 9;
	}
}

static
void emodop()
{
	if ( !os9_crc ) {
		error("orphan %s", oprptr->mnemonic);
		clearaddress();
		return;
	}
	os9_crc = 0;
	put1byte(~os9_u[0]);
	put1byte(~os9_u[1]);
	put1byte(~os9_u[2]);
}

static
void os9op()
{
	skipspace();
	none();
	putbyte(expression());
}
#endif

/* always output 6 bytes */
#if	WITH_TIME
static
void dtbop()
{
	time_t t;
	struct tm *s, z;
	if ( (t=time(NULL))==((time_t)-1) || (s=localtime(&t))==NULL ) {
		error("can't get system time");
		z.tm_sec = z.tm_min = z.tm_hour = z.tm_mon = z.tm_year = 0;
		z.tm_mday = 1;
		s = &z;
	}
	put1byte(s->tm_year);
	put1byte(s->tm_mon+1);
	put1byte(s->tm_mday);
	put1byte(s->tm_hour);
	put1byte(s->tm_min);
	put1byte(s->tm_sec);
}

/* always output 24 bytes, ascii, no control character */
static
void dtsop()
{
	const char *s;
	unsigned char c;
	int i, len;
	time_t t;
	if ( (t=time(NULL))==((time_t)-1) || (s=ctime(&t))==NULL ) {
		error("can't get system time");
		len = 0;
	}
	else
		len = strlen(s);
	for ( i=0; i<24; i++ ) {
		if ( i >= len || (c=*s++) < ' ' )
			c = ' ';
		put1byte(c);
	}
}

#endif

#if	WITH_EXTRA
static
void fillop()
{
	int c, i;
	skipspace();
	c = expression();
	if ( !checkchar(',') ) {
		error("missing argument");
		return;
	}
	noundef = 1;
	i = expression();
	if ( !valid ) {
		sizeinvalid = 1;
		return;
	}
	if ( i < 0 ) {
		error("%s count value %d is negative", oprptr->mnemonic, i);
		return;
	}
	for ( ; i>0; i-- )
		put1byte(c);
}
#endif

