/*
 * Copyright (C) 2019-2021 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  cond.c --- conditional
 *
 */


static CONDSTACK *p1condstk;

typedef struct ifp1 {
	int lc;
	int dlc;
	int totsize;
} IFP1;

static IFP1 pre_ifp1;

static
void ifp1_enter(void)
{
	if (!inp1) {
		inp1 = 1;
		p1condstk = condstk;
		pre_ifp1.lc = lc;
		pre_ifp1.dlc = dlc;
		pre_ifp1.totsize = totsize;
		/*lc = dlc = totsize = 0;*/
	}
}

static
void ifp1_leave(void)
{
	inp1 = 0;
	p1condstk = NULL;
	lc = pre_ifp1.lc;
	dlc = pre_ifp1.dlc;
	totsize = pre_ifp1.totsize;
}

static
void cond_push(void)
{
	CONDSTACK *newconds;
	newconds = (CONDSTACK*)as_calloc(1, sizeof(CONDSTACK));
	newconds->prev = condstk;
	newconds->cond = cond;
	condstk = newconds;
}

static
void cond_pop(void)
{
	CONDSTACK *prevconds;
	if (condstk == p1condstk)
		ifp1_leave();
	prevconds = condstk->prev;
	cond = condstk->cond;
	as_free(condstk);
	condstk = prevconds;
}

static
void cond_free(int quiet)
{
	if (condstk) {
		if (!quiet)
			error("unterminated conditional");
		do cond_pop(); while (condstk);
	}
}

/* extract 2 arguments, the rightmost unenclosed comma
   is the argument separator. */
static
char getargs(char **text1, char **text2)
{
	char *beg, *arg[2], c, esc, err, sep;
	int par;
	arg[0] = arg[1] = NULL;
	esc = err = 0;
	par = 0;
	for (beg=lineptr; (c=*lineptr); lineptr++) {
		switch (esc) {
		case 0:
			switch (c) {
			case '\'':
			case '\"':
				esc = c;
				break;
			case '(':
				par++;
				break;
			case ')':
				if (!par) {
					err = 1;
					goto end;
				}
				par--;
				break;
			default:
				sep = isseparator(c);
				if (sep || (c == ',' && !par)) {
					arg[0] = arg[1];
					arg[1] = lineptr;
					if (sep)
						goto end;
					break;
				}
			}
			break;
		case '\'':
		case '\"':
			if (c == esc)
				esc = 0;
			break;
		default:
			if (c == EOL)
				goto end;
			break;
		}
	}
end:	if (err) {
		error("missing '(' in argument");
	}
	else if (par) {
		error("missing ')' in argument");
		err = 1;
	}
	else if (esc) {
		error("unterminated %c", esc);
		err = 1;
	}
	else if (!arg[0] || !arg[1]) {
		error("missing argument");
		err = 1;
	}
	if (!err) {
		*text1 = as_strndup(beg, arg[0]-beg);
		*text2 = as_strndup(arg[0]+1, arg[1]-arg[0]-1);
	}
	return err;
}

static
char istextequal(char *text1, char *text2, int invert)
{
	char val;
	val = !strcmp(text1, text2) == !invert;
	as_free(text1);
	as_free(text2);
	return val;
}

/*****************************************************************************/
/* optab function */

static
void condop(void)
{
	int expr1, expr2;
	char *text1, *text2, val, err;
	unsigned char opcode, cc;
	opcode = (unsigned char)oprptr->opcode;
	if (opcode == COND_END) {
		if (!condstk)
			error("%s without matching IF", oprptr);
		else
			cond_pop();
	}
	else {
		DEBUG(printbyte(cond,LOD+0);)
		if (opcode & COND_BIT_ELSE) {
			if (!condstk) {
				error("%s without matching IF", oprptr);
				return;
			}
			else {
				cc = cond & COND_CC_MASK;
				if (cc == COND_IFP1 || cc == COND_IFP2) {
					if (totpass == 1)
						error("%s not allowed on IFP1/IFP2", oprptr);
					cond = COND_FALSE | COND_BIT_FORCE | COND_BIT_ELSE;
				}
				else if (COND_TYPE(cond) == COND_BIT_ELSE) {
					error("%s preceded by ELSE", oprptr);
					cond = COND_FALSE | COND_BIT_FORCE | COND_BIT_ELSE;
				}
				else {
					if (!(cond & COND_BIT_FORCE))
						cond ^= COND_BIT_VALUE;
					cond = (cond & (COND_BIT_VALUE | COND_BIT_FORCE)) | COND_BIT_ELSE;
				}
			}
		}
		DEBUG(printbyte(cond,LOD+3);)
		if (opcode & COND_BIT_IF) {
			if (!(opcode & COND_BIT_ELSE))
				cond_push();
			if (COND_IS_FALSE(cond))
				cond = COND_FALSE | COND_BIT_FORCE;
			else {
				expr1 = expr2 = err = 0;
				text1 = text2 = NULL;
				cc = opcode & COND_CC_MASK;
				switch (cc) {
				case COND_IFDEF: case COND_IFNDEF:
					skipspace();
					expr1 = !reflabel(REF_DEFINED);
					break;
				case COND_IFREF: case COND_IFNREF:
					skipspace();
					expr1 = !reflabel(REF_REFERENCED);
					break;
				case COND_IFC: case COND_IFNC:
					skipspace();
					err = getargs(&text1, &text2);
					break;
				case COND_IFP1: case COND_IFP2:
					break;
				default:
					skipspace();
					noundef = 1+optundefcond;
					expr1 = expression();
					err = experr | !valid;
					if (checkchar(',')) {
						expr2 = expression();
						err |= experr | !valid;
					}
					break;
				}
				if (err)
					cond = COND_FALSE | COND_BIT_FORCE;
				else {
					switch (cc) {
					case COND_IFDEF:
					case COND_IFREF:
					case COND_IFEQ:   val = expr1 == expr2;               break;
					case COND_IFGE:   val = expr1 >= expr2;               break;
					case COND_IFGT:   val = expr1 >  expr2;               break;
					case COND_IFLE:   val = expr1 <= expr2;               break;
					case COND_IFLT:   val = expr1 <  expr2;               break;
					case COND_IFNDEF:
					case COND_IFNREF:
					case COND_IFNE:   val = expr1 != expr2;               break;
					case COND_IFC:    val = istextequal(text1, text2, 0); break;
					case COND_IFNC:   val = istextequal(text1, text2, 1); break;
					case COND_IFP1:   val = totpass == 1 || onepass;
						          if (val) { ifp1_enter(); }          break;
					case COND_IFP2:   val = totpass > 1 || onepass;       break;
					default:          val = 0;                            break;
					}
					cond = opcode | (val ? COND_TRUE : COND_FALSE);
				}
			}
		}
		DEBUG(printbyte(cond,LOD+6);)
		NDEBUG(printdigit(COND_IS_TRUE(cond),LOD);)
	}
}

/*****************************************************************************/
