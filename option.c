/*
 * Copyright (C) 2021 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  option.c --- option processing
 *
 */


static char optexp, def_optexp;
static char optorg, def_optorg;
static char optaup, def_optaup;
static char optdatsep, def_optdatsep;
static char optnodot, def_optnodot;
static char optundefcond, def_optundefcond;
static char optundefset, def_optundefset;
static char optvalrange, def_optvalrange;
static char optigncase, def_optigncase;
static char optcwd, def_optcwd;
static char optautopar, def_optautopar;
static char optlock, def_optlock;
#ifdef	MC6809
static char optlongbranch, def_optlongbranch;
static char optpc, def_optpc;
#endif

typedef struct option {
		char *var;
		char *defvar;
		void (*callback)(void);
		char name[2];
		char min;
		char max;
} OPTION;

static
void optexpcb(void)
{	
	expression = optexp ? expression1 : expression0;
}

static
void optigncasecb(void)
{
	if (optigncase)
		findsymbol = tree_findi;
	else
		findsymbol = tree_find;
}

static
void optlockcb(void)
{
	if (optlock) {
		def_optexp = optexp;
		def_optorg = optorg;
		def_optaup = optaup;
		def_optdatsep = optdatsep;
		def_optnodot = optnodot;
		def_optundefcond = optundefcond;
		def_optundefset = optundefset;
		def_optvalrange = optvalrange;
		def_optigncase = optigncase;
		def_optcwd = optcwd;
		def_optautopar = optautopar;
		def_optlock = optlock;
#ifdef	MC6809
		def_optlongbranch = optlongbranch;
		def_optpc = optpc;
#endif
	}
}

static
void initoptions(void)
{
	optexp = def_optexp;
	optorg = def_optorg;
	optaup = def_optaup;
	optdatsep = def_optdatsep;
	optnodot = def_optnodot;
	optundefcond = def_optundefcond;
	optundefset = def_optundefset;
	optvalrange = def_optvalrange;
	optigncase = def_optigncase;
	optcwd = def_optcwd;
	optautopar = def_optautopar;
#ifdef	MC6809
	optlongbranch = def_optlongbranch;
	optpc = def_optpc;
#endif
	optexpcb();
	optigncasecb();
}

/* Options default value can be set on the command line,
   otherwise set to zero. */
/* Options are reset to default values at the beginning of each pass. */
static
const OPTION options[] =
{
	{ &optexp, &def_optexp, optexpcb, { 'E', 'P'}, 0, 1 },
	/* EP - Expression Parser
	 *  0 = Pparser with operator precedence. (default)
	 *  1 = Parser without operator precedence.
	 */
	{ &optorg, &def_optorg, NULL, { 'O', 'B' }, 0, 1 },
	/* OB - Org Behavior
	 *  0 = ORG address optional, offset optional. (default)
	 *  1 = ORG address mandatory, offset optional and reset
	 *      to zero if not specified.
	 */
	{ &optaup, &def_optaup, NULL, { 'A', 'U' }, 0, 1 },
	/* AU - Address Update
	 *  0 = Address updated on each byte/word emitted,
	 *      apply only to FCB/FDB directive. (default)
	 *  1 = No address update.
	 */
	{ &optdatsep, &def_optdatsep, NULL, { 'D', 'S' }, 0, 1 },
	/* DS - Data Separation
	 *  0 = Data and code uses same adress. (default)
	 *  1 = Data separated from code, ORG/RMB uses
	 *      an alternate address space,
	 *      a single dot can read that address.
	 */
	{ &optnodot, &def_optnodot, NULL, { 'N', 'D' }, 0, 1 },
	/* ND - No Dot
	 *  0 = Turn on special dot processing in label. (default)
	 *  1 = Turn off special dot processing in label,
	 *      no local label, no namespace.
	 */
	{ &optundefcond, &def_optundefcond, NULL, { 'U', 'C' }, 0, 1 },
	/* UC - Undefined Conditional
	 *  0 = Disallow undefined symbol in conditional expression. (default)
	 *  1 = Allow undefined symbol in conditional expression,
	 *      undefined symbol resolv to zero.
	 */
	{ &optundefset, &def_optundefset, NULL, { 'U', 'S' }, 0, 1 },
	/* US - Undefined Symbol
	 *  0 = Disallow undefined symbol referenced by SET. (default)
	 *  1 = Allow undefined symbol referenced by SET,
	 *      undefined symbol resolv to zero.
	 */
	{ &optvalrange, &def_optvalrange, NULL, { 'V', 'R' }, 0, 1 },
	/* VR - Value Range
	 *  0 = Error when the value is out of range. (default)
	 *  1 = No error when the value is out of range.
	 */
	{ &optigncase, &def_optigncase, optigncasecb, { 'I', 'C' }, 0, 1 },
	/* IC - Ignore Case
	 *  0 = Symbol case is not ignored. (default)
	 *  1 = Symbol case is ignored.
	 */
	{ &optcwd, &def_optcwd, NULL, { 'C', 'D' }, 0, 1 },
	/* CD - Current Directory
	 *  0 = Don't search file in current working directory. (default)
	 *  1 = Also try to search for files in the current working directory.
	 */
	{ &optautopar, &def_optautopar, NULL, { 'A', 'P' }, 0, 1 },
	/* AP - Auto Parent
	 *  0 = Label without dot sets parent label. (default)
	 *  1 = No automatic parent label.
	 *     (This option is meaningful only if ND=0)
	 */
	{ &optlock, &def_optlock, optlockcb, { 'L', 'O' }, 0, 1 },
	/* LO - Lock Options
	 *  0 = Do nothing. (default)
	 *  1 = Make all options the new default and prevent further
	 *      modification of the options, will silently ignore the OPT
	 *      directive.
	 */
#ifdef	MC6809
	{ &optlongbranch, &def_optlongbranch, NULL, { 'L', 'B' }, 0, 1 },
	/* LB - Long Branch
	 *  0 = No optimization on long branch. (default)
	 *  1 = Optimize long branch.
	 *  (This option overwrite -B command line option)
	 */
	{ &optpc, &def_optpc, NULL, { 'P', 'C' }, 0, 1 },
	/* PC - Program Counter
	 *  0 = PC in indexed mode is independent from the
	 *      assembler current address. (default)
	 *  1 = PC in indexed mode is relative to the
	 *      assembler current address, like PCR.
	 */
#endif
	{ NULL, NULL, NULL, { 0, 0 }, 0, 0 },
};

static
char *parseoption(char *lineptr, int def)
{	char val, opt[3];
	const OPTION *option;
	if (!def_optlock) for (;;) {
		if (isalphanumeric(lineptr[0]) && isalphanumeric(lineptr[1]) && lineptr[2]=='=') {
			opt[0] = toupper(lineptr[0]); opt[1] = toupper(lineptr[1]); opt[2] = 0;
			for (option=options; ; option++) {
				if (!option->var) {
					error("unrecognizable option '%s'", opt);
					break;
				}
				else if (opt[0] == option->name[0] && opt[1] == option->name[1]) {
					val = lineptr[3] - '0';
					if (val < option->min || val > option->max)
						error("bad value at option '%s'", opt);
					else {
						if (def)
							*option->defvar = val;
						*option->var = val;
						if (option->callback)
							option->callback();
						if (def_optlock)
							goto end;
					}
					break;
				}
			}
		}
		else {
			error("bad option");
			break;
		}
		lineptr += 4;
		if (*lineptr != ',')
			break;
		lineptr++;
	}
	if (def_optlock)
end:		while ( *lineptr && !isseparator(*lineptr) ) lineptr++;
	if (def && *lineptr)
		error("bad option");
	return lineptr;
}

