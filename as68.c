/*
 * Copyright (C) 1981, 1987 Masataka Ohta, Hiroshi Tezuka
 * Copyright (C) 2016-2021 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  as68.c --- define the whole program
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "as68.h"
#if	WITH_TIME
#include <time.h>
#endif
#include "tree.c"
#include "option.c"
#include "oppro.c"
#include "optab.c"
#include "main.c"
#include "symtab.c"
#include "srctab.c"
#include "eval0.c"
#include "eval1.c"
#include "obj.c"
#include "util.c"
#include "alloc.c"
#include "cond.c"
#include "macro.c"
#include "globl.c"
#if	WITH_STRUCT
#include "struct.c"
#endif
