/*
 * Copyright (C) 2021 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  macro.h --- macro handling
 *
 */


/* structure */

typedef struct ml {
	struct ml *next;
	char *line;
} MACROLINE;

typedef struct mt {
	TREE t;
	struct mt *frommacro;
	SRCTABLE *fromsource;
	MACROLINE *lines, *tail;
	int lineno;
	char expand;
} MACROTABLE;

typedef struct mp {
	char *operand;
	char **param;
	int instance;
	int number;
} MACROPARAM;

typedef struct ms {
	struct ms *prev;
	MACROTABLE *macro;
	MACROLINE *line;
	MACROPARAM *param;
	int lineno;
	unsigned char cond;
	CONDSTACK *condstk;
} MACROSTACK;


/* variable */
static char expand;
static unsigned int macro_defs, macro_skip;
static MACROSTACK *macrostk;


/* prototype */
static void endmop(void), exitop(void), macroop(void),
	macro_addline(const char *line), macro_free(void), macro_init(void),
	macro_popfile(void), macro_enter(MACROTABLE *mt);
static char *macro_getline(char *buffer, int size);
static MACROTABLE *macro_check(const char *name);
#ifdef	CLEANUP
static void macro_cleanup(void);
#endif


