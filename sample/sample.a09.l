   1                    *******************************************************************
   2                    * This is an assembler test/sample for as09,
   3                    * to show some functionality, it will compile
   4                    * without errors, but it does nothing useful.
   5                    *
   6                    
   7                    
   8                    ****************************
   9                    * comments
  10                    * comment style 1
  11                    ; comment style 2
  12                    # comment style 3
  13                            ; comment
  14 0000               label   ; comment
  15 0000 12                    nop     implicit comment
  16 0001 86   00               lda     #0      implicit comment
  17                    
  18                    
  19                    ****************************
  20                    * labels
  21 0003 B6   000A     func1   lda     .var
  22 0006 4A            .loop   deca
  23 0007 26   FD               bne     .loop
  24 0009 39                    rts
  25 000A      0001     .var    rmb     1
  26                    
  27 000B B6   0012     func2   lda     .var
  28 000E 4C            .loop   inca
  29 000F 26   FD               bne     .loop
  30 0011 39                    rts
  31 0012      0001     .var    rmb     1
  32 0013      0001     .a      rmb     1
  33                    
  34                            with    func3
  35 0014 4C            .a      inca
  36 0015 4C            .b      inca
  37 0016 4C            .c      inca
  38 0017 39                    rts
  39                    
  40           0023     func1.a equ     $23
  41                    
  42 0018 C6   23               ldb     #func1.a
  43 001A D7   0A               stb     func1.var
  44 001C 9D   03               jsr     func1
  45 001E 9D   14               jsr     func3.a
  46 0020 9D   15               jsr     func3.b
  47 0022 9D   16               jsr     func3.c
  48                    
  49                            with    func2
  50 0024 D6   13               ldb     .a              ; equiv to:             ldb func2.a
  51 0026 D7   12               stb     .var            ; equiv to:             stb func2.var
  52           000C     .b      equ     12              ; equiv to: func2.b     equ 12
  53                    
  54                            with    func3
  55 0028 9D   14               jsr     .a
  56 002A 9D   15               jsr     .b
  57 002C 9D   16               jsr     .c
  58                    
  59                    
  60                    ****************************
  61                    * expression parser
  62           0007     const1  equ     1+2*3           ; const1 = 7 (using default option value)
  63                            opt     ep=0            ; with operator precedence
  64           0007     const2  equ     1+2*3           ; const2 = 7
  65                            opt     ep=1            ; without operator precedence
  66           0009     const3  equ     1+2*3           ; const3 = 9
  67                            opt     ep=0
  68                            ; logical not
  69           0000     const4  equ     !123            ; const4 = 0
  70           0001     const5  equ     !const4         ; const5 = 1
  71                    
  72                    
  73                    ****************************
  74                    * ascii literal constant
  75           0041     ac01    equ        'A           ; ac01 = $0041
  76           0041     ac02    equ        'A'          ; ac02 = $0041 (alternate form)
  77           4142     ac03    equ        "AB          ; ac03 = $4142
  78           4142     ac04    equ        "AB"         ; ac04 = $4142 (alternate form)
  79           4146     ac05    equ        "AB+4        ; ac05 = $4146
  80           4146     ac06    equ        "AB"+4       ; ac06 = $4146
  81                            ; beware of these, they are
  82                            ; valid but may seem ambiguous
  83           0020     ac07    equ        '            ; ac07 = $20
  84           0027     ac08    equ        ''           ; ac08 = $27
  85           4120     ac09    equ        "A           ; ac09 = $4120
  86           4122     ac10    equ        "A"          ; ac10 = $4122
  87           2220     ac11    equ        ""           ; ac11 = $2220
  88           2222     ac12    equ        """          ; ac12 = $2222
  89                    
  90                    
  91                    ****************************
  92                    * strings and bytes
  93                            ; single byte
  94 002E 61 62 63              fcb     "abc"                   ; 61 62 63
  95 0031 61 62 63 FF           fcb     "abc",-1                ; 61 62 63 FF
  96 0035 31 32 33 34           fcb     "12",'3,"45",'6',-1     ; 31 32 33 34 35 36 FF
     0039 35 36 FF      
  97 003C 01 02 03              fcb     ,,,1,,,,2,,,3,          ; 01 02 03
  98 003F                       fcb     ,,,,                    ;
  99                            ; double byte
 100 003F 0061 0062             fdb     "abc"                   ; 0061 0062 0063
     0043 0063          
 101 0045 0061 0062             fdb     "abc",-1                ; 0061 0062 0063 FFFF
     0049 0063 FFFF     
 102 004D 0001 0002             fdb     ,,,1,,,,2,,,3,          ; 0001 0002 0003
     0051 0003          
 103 0053                       fdb     ,,,,                    ;
 104                            ; null terminated string
 105 0053 61 62 63 00           fcn     "abc"                   ; 61 62 63 00
 106 0057 00                    fcn     ""                      ; 00
 107 0058 00 61 00 00           fcn     "","a",""               ; 00 61 00 00
 108 005C                       fcn     ,                       ;
 109                            ; bit-7 of last byte of each string set to one
 110 005C 61 62 E3              fcs     "abc"                   ; 61 62 E3
 111 005F 61 62 E3 31           fcs     "abc","123"             ; 61 62 E3 31 32 B3
     0063 32 B3         
 112 0065 80                    fcs     ""                      ; 80
 113 0066                       fcs     ,                       ;
 114                            ; comma can be used for padding source code
 115 0066                       fcn     ,,,,,,,,,,,,,,,,,,,,,,,,,
 116 0066 41 42 43 00           fcn     ,"ABC",,"ABC",,,,,,"ABC",
     006A 41 42 43 00   
     006E 41 42 43 00   
 117 0072 41 42 43 00           fcn     ,"ABC",,"1",,,,,,,,"1",,,
     0076 31 00 31 00   
 118 007A 31 00 41 42           fcn     ,"1",,,,"ABCEFGH",,"ABC",
     007E 43 45 46 47   
     0082 48 00 41 42   
     0086 43 00         
 119 0088 00 00 31 00           fcn     ,"",,,,,"",,,,,,,,,"1",,,
 120 008C                       fcn     ,,,,,,,,,,,,,,,,,,,,,,,,,
 121                            ; same as
 122 008C 41 42 43 00           fcn     "ABC","ABC","ABC"
     0090 41 42 43 00   
     0094 41 42 43 00   
 123 0098 41 42 43 00           fcn     "ABC","1","1"
     009C 31 00 31 00   
 124 00A0 31 00 41 42           fcn     "1","ABCEFGH","ABC"
     00A4 43 45 46 47   
     00A8 48 00 41 42   
     00AC 43 00         
 125 00AE 00 00 31 00           fcn     "","","1"
 126                    
 127                    ****************************
 128                    * fill
 129                            ; single byte
 130 00B2 00 00 00              flb     3                       ; 00 00 00
 131 00B5 05 05 05              flb     3,5                     ; 05 05 05
 132 00B8 05 06 07              flb     3,5,1                   ; 05 06 07
 133                            ; double byte
 134 00BB 0000 0000             fld     3                       ; 0000 0000 0000
     00BF 0000          
 135 00C1 0005 0005             fld     3,5                     ; 0005 0005 0005
     00C5 0005          
 136 00C7 FFF0 FFEE             fld     4,-16,-2                ; FFF0 FFEE FFEC FFEA
     00CB FFEC FFEA     
 137                    
 138                    
 139                    ****************************
 140                    * operand size and offset
 141 00CF E6   84               ldb     ,x                      ; E6   84
 142 00D1 E6   84               ldb     0,x                     ; E6   84
 143 00D3 E6   84               ldb     ^offset0,x              ; E6   84
 144 00D5 E6   01               ldb     ^offset1,x              ; E6   01
 145 00D7 E6   88 00            ldb     <offset0,x              ; E6   88 00
 146 00DA E6   88 01            ldb     <offset1,x              ; E6   88 01
 147 00DD E6   89 0000          ldb     >offset0,x              ; E6   89 0000
 148 00E1 E6   89 0001          ldb     >offset1,x              ; E6   89 0001
 149 00E5 E6   89 0000          ldb     offset0,x               ; E6   89 0000
 150 00E9 E6   89 0001          ldb     offset1,x               ; E6   89 0001
 151           0000     offset0 equ     0
 152           0001     offset1 equ     1
 153 00ED E6   84               ldb     offset0,x               ; E6   84
 154 00EF E6   01               ldb     offset1,x               ; E6   01
 155                    
 156                    
 157                    *******************************************************************
     Total Errors 0
     Total Labels 32
