/*
 * Copyright (C) 1981, 1987 Masataka Ohta, Hiroshi Tezuka
 * Copyright (C) 2016-2019 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  alloc.c --- memory allocation
 *
 */

static
void *as_malloc(unsigned int len)
{
	void *newmem;
	newmem = malloc(len);
DEBUG(fprintf(stderr, "as_malloc(%u)=%p\n", len, newmem);)
	if ( !newmem )
		fatal("out of memory",NULL,OOMERROR,0);
	return newmem;
}

static
void as_free(void *mem)
{
DEBUG(fprintf(stderr, "as_free(%p)\n", mem);)
	if ( mem )
		free(mem);
}

static
void *as_calloc(unsigned int nmemb, unsigned int size)
{
	void *newmem;
	unsigned int len;
	len = nmemb * size;
	newmem = as_malloc(len);
	memset(newmem, 0, len);
	return newmem;
}

static
char *as_strconcat5(const char *str1, const char *str2, const char *str3, const char *str4, const char *str5)
{
	char *newstr;
	int len, len1, len2, len3, len4, len5;
	len1 = strlen(str1);
	len2 = strlen(str2);
	len3 = strlen(str3);
	len4 = strlen(str4);
	len5 = strlen(str5);
	newstr = (char *)as_malloc(len1 + len2 + len3 + len4 + len5 + 1);
	memcpy(newstr,     str1, len1); len=len1;
	memcpy(newstr+len, str2, len2); len+=len2;
	memcpy(newstr+len, str3, len3); len+=len3;
	memcpy(newstr+len, str4, len4); len+=len4;
	memcpy(newstr+len, str5, len5); len+=len5;
	newstr[len] = 0;
	return newstr;
}

static
char *as_strconcat3(const char *str1, const char *str2, const char *str3)
{
	return as_strconcat5(str1, str2, str3, "", "");
}

static
char *as_strconcat2(const char *str1, const char *str2)
{
	return as_strconcat5(str1, str2, "", "", "");
}

static
char *as_strndup(const char *str, int len)
{
	char *newstr;
	newstr = (char *)as_malloc(len + 1);
	memcpy(newstr, str, len);
	newstr[len] = 0;
	return newstr;
}

static
char *as_strdup(const char *str)
{
	return as_strndup(str, strlen(str));
}
