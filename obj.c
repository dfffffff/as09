/*
 * Copyright (C) 1981, 1987 Masataka Ohta, Hiroshi Tezuka
 * Copyright (C) 2016-2021 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  obj.c --- SREC/IHEX/RAW/DECB/OS9 object file output
 *
 */

#if	DECB_FORMAT
static long decboffset;
static int decbsize, decbobjlc, decbonce, decbnoopt;

static
void decbheader(int op, int size, int addr)
{
	if ( !decbnoopt )
		size = 0;
	if ( !decbnoopt && decbsize != 0 ) {
		fseek(objf, decboffset, SEEK_SET);
		putc(decbsize>>8,objf);	/* size hi */
		putc(decbsize,objf);	/* size lo */
		fseek(objf, 0, SEEK_END);
		decbsize = 0;
	}
	putc(op,objf);
	decboffset = ftell(objf);
	putc(size>>8,objf);			/* size hi */
	putc(size,objf);			/* size lo */
	putc(addr>>8,objf);			/* addr hi */
	putc(addr,objf);			/* addr lo */
}
#endif

static
int putw1(int w)
{	putb(w >> 8);
	putb(w);
	return w;
}

static
int putb(int b)
{
#if	WITH_OS9
	unsigned int t;
	unsigned char s;
#endif
	if ( pass == 0 )
	{
#if	WITH_OS9
		if ( os9_crc ) {
			s = (unsigned char)b;
			s ^= os9_u[0];
			os9_u[0] = os9_u[1];
			os9_u[1] = os9_u[2];
			t = (unsigned int)s << 1;
			os9_u[1] ^= (unsigned char)(t >> 8);
			os9_u[2] = (unsigned char)t;
			t = (unsigned int)s << 6;
			os9_u[1] ^= (unsigned char)(t >> 8);
			os9_u[2] ^= (unsigned char)t;
			s ^= s << 1;
			s ^= s << 2;
			s ^= s << 4;
			if (s & 0x80) {
				os9_u[0] ^= 0x80;
				os9_u[2] ^= 0x21;
			}
		}
#endif
		if ( OBJSIZE <= objpos || ((lc + objbias) & 0xFFFF) != objnext )
			flushobj();
		objnext = (lc + objbias + 1) & 0xFFFF;
		objbuf[objpos++] = (char)b;
	}
	lc++;
	return b;
}

static
void flushobj()
{	int i;
	if ( objpos > 0 && object )
	{	chksum = 0;
#if	RAW_FORMAT || IHEX_FORMAT || DECB_FORMAT
		switch (format) {
#if	RAW_FORMAT
		case RAW_FORMAT:
			for ( i = 0; i < objpos; i++ ) putc(objbuf[i],objf);
			break;
#endif
#if	IHEX_FORMAT
		case IHEX_FORMAT:
			putc(':',objf);
			put2hex(objpos);
			put4hex(objlc);
			put2hex(0);
			for ( i = 0; i < objpos; i++ ) put2hex(objbuf[i]);
			put2hex(-chksum);
			putc('\n',objf);
			break;
#endif
#if	DECB_FORMAT
		case DECB_FORMAT:
			if ( objlc != decbobjlc || !decbonce || decbnoopt ) {
				if ( !decbonce ) {
					decbonce = 1;
					if (ftell(objf) == -1)	/* happen on non-file (e.g. pipe) */
						decbnoopt = 1;	/* disable optimization */
				}
				decbheader(0x00,objpos,objlc);
			}
			decbobjlc = (objlc + objpos) & 0xFFFF;
			decbsize += objpos;
			for ( i = 0; i < objpos; i++ ) putc(objbuf[i],objf);
			break;
#endif
		default:
#endif
		/* SREC_FORMAT */
		fputs("S1",objf);
		put2hex(objpos + 3);
		put4hex(objlc);
		for ( i = 0; i < objpos; i++ ) put2hex(objbuf[i]);
		put2hex(~chksum);
		putc('\n',objf);
#if	RAW_FORMAT || IHEX_FORMAT || DECB_FORMAT
		}
#endif
	}
	objpos = 0;
	objlc = (lc + objbias) & 0xFFFF;
}

static
void put2hex(int b)
{	putc(hexdigit(b >> 4),objf);
	putc(hexdigit(b),objf);
	chksum += b;
}

static
void put4hex(int w)
{	put2hex(w >> 8);
	put2hex(w);
}

static
void termobj()
{	flushobj();
#if	RAW_FORMAT || IHEX_FORMAT || DECB_FORMAT
	switch (format) {
#if	RAW_FORMAT
	case RAW_FORMAT:
		break;
#endif
#if	IHEX_FORMAT
	case IHEX_FORMAT:
		fputs(":00",objf);
		chksum = 0;
		put4hex(startadr);
		put2hex(1);
		put2hex(-chksum);
		putc('\n',objf);
		break;
#endif
#if	DECB_FORMAT
	case DECB_FORMAT:
		decbheader(0xFF,0,startadr);
		break;
#endif
	default:
#endif
	/* SREC_FORMAT */
	fputs("S903",objf);
	chksum = 3;
	put4hex(startadr);
	put2hex(~chksum);
	putc('\n',objf);
#if	RAW_FORMAT || IHEX_FORMAT || DECB_FORMAT
	}
#endif
}
