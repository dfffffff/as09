/*
 * Copyright (C) 2021 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  globl.c --- global symbol
 *
 */


static
int globl_exist(const char *name)
{
	if (FIND_GLOBL(name, &globltbl, NULL))
		return 1;
	if (FIND_GLOBL(name, &privglobltbl, NULL))
		return 1;
	return 0;
}


static
GLOBLTABLE *globl_add(char *name, char flag)
{
	GLOBLTABLE *glbl, **glblp, **table;
	table = (flag & GLOBAL_TYPE_MASK) == GLOBAL_TYPE_GLOBAL ? &globltbl : &privglobltbl;
	if (!FIND_GLOBL(name, table, &glblp)) {
		glbl = (GLOBLTABLE *)as_calloc(1, sizeof(GLOBLTABLE));
		glbl->t.name = name;
		glbl->flag = flag;
		*glblp = glbl;
		return glbl;
	}
	return NULL;
}


static
void globl_freenode(GLOBLTABLE *gt)
{
	if (gt != NULL) {
		globl_freenode((GLOBLTABLE*)gt->t.right);
		globl_freenode((GLOBLTABLE*)gt->t.left);
		if (gt->flag & GLOBAL_FLAG_FREE)
			as_free(gt->t.name);
		as_free(gt);
	}
}


static
void globl_freepriv(void)
{
	globl_freenode(privglobltbl);
	privglobltbl = NULL;
}


#ifdef	CLEANUP
static
void globl_cleanup(void)
{
	globl_freenode(globltbl);
	globl_freenode(privglobltbl);
}
#endif


/*****************************************************************************/
/* optab function */

static
void globlop(void)
{
	char *label, type;
	type = oprptr->opcode;
	skipspace();
	do {
		label = getlabel();
		if (!*label || !globl_add(label, type))
			as_free(label);
	} while (checkchar(','));
}

/*****************************************************************************/
