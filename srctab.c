/*
 * Copyright (C) 2021 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  srctab.c --- source table
 *
 */


static
void src_set(const char *filename)
{
	static unsigned int index;
	SRCTABLE *st, **stp;
	st = (SRCTABLE*)tree_find(filename, (TREE**)&srctab, (TREE***)&stp);
	if ( !st ) {
		st = *stp = (SRCTABLE *)as_calloc(1, sizeof(SRCTABLE));
		st->t.name = as_strdup(filename);
		st->index = ++index;
	}
	cursrc = st;
}

static
void src_resetinst(SRCTABLE *st)
{
	if ( st != NULL ) {
		if ( st->t.right )
			src_resetinst((SRCTABLE*)st->t.right);
		if ( st->t.left )
			src_resetinst((SRCTABLE*)st->t.left);
		st->instance = 0;
	}
}

#ifdef	CLEANUP
static
void src_free(SRCTABLE *st)
{
	if ( st != NULL ) {
		src_free((SRCTABLE*)st->t.right);
		src_free((SRCTABLE*)st->t.left);
		as_free(st->t.name);
		as_free(st);
	}
}
#endif
