/*
 * Copyright (C) 2021 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  struct.c --- structure handling
 *
 */


#define STRUCT_SEP "."
#define SIZEOF_EQUATE " EQU .-"
#define START_EQUATE " EQU ."
#define MAX_NESTED_STRUCT ((MAXLINESZ-(sizeof(SIZEOF_TAG)-1)-(sizeof(SIZEOF_EQUATE)-1)-1)/((MNEMOBUFSIZE+1)*2)-1)
#define FIND_STRUCT(X,Y,Z) (STRUCTTABLE*)findsymbol((X),(TREE**)(Y),(TREE***)(Z))


static STRUCTTABLE *structtab, *def_struct;
static int struct_lc, struct_dlc, struct_totsize;
static int struct_llc, struct_dllc;
static char *struct_def_name;
static unsigned int struct_skip;
static unsigned char struct_sdis_level;


static
void struct_init(void)
{
	struct_skip = struct_defs = 0;
	struct_level = struct_sdis_level = 0;
	struct_from_def = 0;
	def_struct = NULL;
	structstk = NULL;
}


static
void struct_push(STRUCTTABLE *structure, char *name, char struct_in_struct)
{
	STRUCTSTACK *ss;
	ss = (STRUCTSTACK *)as_calloc(1, sizeof(STRUCTSTACK));
	ss->prev = structstk;
	ss->structure = structure;
	ss->line = structure->lines;
	ss->name = name;
	if (struct_in_struct) {
		if (ss->prev->linep)
			ss->linep = ss->prev->linep;
		else
			ss->linep = &ss->prev->line;
		ss->start = 1;
		struct_sdis_level++;
	}
	structstk = ss;
	struct_level++;
}


static
int struct_pop(void)
{
	STRUCTSTACK *ss;
	ss = structstk;
	if (ss) {
		struct_level--;
		as_free(ss->name);
		as_free(ss->last);
		structstk = ss->prev;
		as_free(ss);
		if (structstk)
			return 1;
		if (struct_sdis_level) {
			struct_sdis_level = 0;
			error("weird structure");
		}
		if (struct_from_def) {
			struct_from_def = 0;
			lc = struct_lc;
			dlc = struct_dlc;
			llc = struct_llc;
			dlc = struct_dllc;
			totsize = struct_totsize;
		}
	}
	return 0;
}


static
void struct_popfile(void)
{
	if (struct_defs)
		error("unterminated structure");
	while (struct_pop());
	as_free(struct_def_name);
	struct_def_name = NULL;
}


static
void struct_appendstr(char **buffer, int *bufrem, const char *str)
{
	int rem;
	rem = *bufrem;
	while (*str) {
		if (rem <= 0)
			fatal("too long structure line expansion",NULL,LTLERROR,2);
		**buffer = *str++;
		(*buffer)++;
		rem--;
	}
	*bufrem = rem;
}


static
void struct_addline(const char *line, unsigned char prefix)
{
	int space;
	STRUCTLINE *newline;
	if (prefix != RMB && prefix != STRC && prefix != COND && prefix != STRD && prefix != ENDS && prefix != OKSD)
		return;
	space = isspace(*line);
	if (prefix == RMB && space) {
		error("unnamed structure member");
		return;
	}
	if (prefix == OKSD && !space) {
		error("label not allowed");
		return;
	}
	if (struct_skip || (prefix == STRD && struct_defs == 1))
		return;
	newline = (STRUCTLINE *)as_calloc(1, sizeof(STRUCTLINE));
	newline->line = as_strdup(line);
	newline->prefix = prefix;
	if (!def_struct->lines)
		def_struct->lines = newline;
	if (def_struct->tail)
		def_struct->tail->next = newline;
	def_struct->tail = newline;
}


static
void struct_emitsize(char **bufptr, int *bufrem, char *line)
{
	char *label, *tlineptr;
	tlineptr = lineptr;
	lineptr = line;
	label = getlabel();
	lineptr = tlineptr;
	struct_appendstr(bufptr, bufrem, label);
	struct_appendstr(bufptr, bufrem, SIZEOF_TAG SIZEOF_EQUATE);
	struct_appendstr(bufptr, bufrem, label);
	struct_appendstr(bufptr, bufrem, EOLS);
	**bufptr = 0;
	as_free(label);
}


static
int struct_getline(char *buffer, int size)
{
	STRUCTLINE **linep;
	char *bufptr, label;
	int bufrem;
	bufptr = buffer;
	bufrem = size;
	if (structstk->start) {
		structstk->start = 0;
		struct_appendstr(&bufptr, &bufrem, structstk->name);
		struct_appendstr(&bufptr, &bufrem, START_EQUATE);
		struct_appendstr(&bufptr, &bufrem, EOLS);
		*bufptr = 0;
		goto success;
	}
	if (!structstk->last) {
		linep = structstk->linep ? structstk->linep : &structstk->line;
		while (!*linep) {
			if (!structstk->end) {
				structstk->end = 1;
				struct_emitsize(&bufptr, &bufrem, structstk->name);
				goto success;
			}
			if (!struct_pop())
				return 0;
		}
		label = (*linep)->prefix != COND && (*linep)->prefix != ENDS && (*linep)->prefix != OKSD;
		if (label) {
			struct_appendstr(&bufptr, &bufrem, structstk->name);
			struct_appendstr(&bufptr, &bufrem, STRUCT_SEP);
		}
		struct_appendstr(&bufptr, &bufrem, (*linep)->line);
		*bufptr = 0;
		if (label)
			structstk->last = as_strdup(buffer);
		*linep = (*linep)->next;
	}
	else {
		struct_emitsize(&bufptr, &bufrem, structstk->last);
		as_free(structstk->last);
		structstk->last = NULL;
	}
success:
	return 1;
}


static
STRUCTTABLE *struct_check(const char *name)
{
	return FIND_STRUCT(name, &structtab, NULL);
}


static
void struct_enter(STRUCTTABLE *st, int def)
{
	char *name, *tlineptr;

	if (struct_level >= MAX_NESTED_STRUCT) {
		error("too many nested structures");
		return;
	}

	tlineptr = lineptr;
	lineptr = linebeg;
	name = getnewlabel(NULL);
	lineptr = tlineptr;
	if (!name)
		return;

	struct_push(st, name, 0);

	if (def) {
		struct_from_def = def;
		struct_lc = lc;
		struct_dlc = dlc;
		struct_llc = llc;
		struct_dllc = dlc;
		struct_totsize = totsize;
		lc = 0;
		dlc = 0;
		llc = 0;
		dllc = 0;
	}
}


#ifdef	CLEANUP
static
void struct_freelines(STRUCTLINE *sl)
{
	STRUCTLINE *next;
	while (sl) {
		next = sl->next;
		as_free(sl->line);
		as_free(sl);
		sl = next;
	}
}

static
void struct_free(STRUCTTABLE *st)
{
	if (st) {
		struct_free((STRUCTTABLE*)st->t.right);
		struct_free((STRUCTTABLE*)st->t.left);
		struct_freelines(st->lines);
		as_free(st->t.name);
		as_free(st);
	}
}

static
void struct_cleanup(void)
{
	struct_sdis_level = 0;
	struct_defs = 0;
	struct_popfile();
	struct_free(structtab);
	structtab = NULL;
}
#endif

/*****************************************************************************/
/* optab functions */

static
void structop(void)
{
	char *name, *tlineptr;
	int tllc, tlcvalid;
	OPTABLE *q;
	STRUCTTABLE *st, **stp;
	char temp[MNEMOSIZE + 1 + 1], *p, *pp, *ptr;

	/* nested structure definition in structure */
	if (structstk) {
		tlineptr = lineptr;
		lineptr = linebeg;
		name = getlabel();
		lineptr = tlineptr;
		struct_push(structstk->structure, name, 1);
		return;
	}

	if (struct_defs++) {
		if (struct_skip)
			struct_skip++;
		return;
	}

	name = NULL;
	if (isspace(*linebeg)) {
		error("%s without name", oprptr->mnemonic);
		goto skip;
	}

	tlineptr = lineptr;
	lineptr = linebeg;
	name = getnewlabel(NULL);
	lineptr = tlineptr;
	if (!name)
		goto skip;

	as_free(struct_def_name);
	struct_def_name = as_strconcat2(name, EOLS);

	st = FIND_STRUCT(name, &structtab, &stp);
	if (st) {
		if (st->srcfile != cursrc || st->srclineno != flineno) {
			error("multiple defined %s", oprptr->mnemonic);
			goto skip;
		}
		struct_skip++;
		goto set;
	}
	else {
		for (ptr = name, p = temp, pp = temp + MNEMOSIZE + 1; p < pp; p++, ptr++)
			if ( !(*p = (char)toupper(*ptr)) ) break;
		*p = 0;
		for (q = ophash[ hash( temp ) ]; q != NULL; q = q->nl) {
			if ( strcmp(temp,q->mnemonic) == 0 ) {
				error("attempt to redefine %s as %s", q->mnemonic, oprptr->mnemonic);
				goto skip;
			}
		}
	}

	*stp = st = (STRUCTTABLE *)as_calloc(1, sizeof(STRUCTTABLE));
	st->t.name = name;
	st->srcfile = cursrc;
	st->srclineno = flineno;
	name = NULL;

set:
	tllc = llc;
	tlcvalid = lcvalid;
	llc = 0;
	lcvalid = 1;
	tlineptr = lineptr;
	lineptr = linebeg;
	deflabel();
	lineptr = tlineptr;
	llc = tllc;
	lcvalid = tlcvalid;

	def_struct = st;

	skipspace();
	st->expand = *lineptr != EOL ? !!expression() : 0;
	as_free(name);
	return;

skip:
	as_free(name);
	as_free(struct_def_name);
	struct_def_name = NULL;
	struct_skip++;
}


static
void endsop(void)
{
	STRUCTTABLE *st;
	char *tlinebeg;

	/* nested structure definition in structure */
	if (structstk) {
		struct_sdis_level--;
		struct_pop();
		return;
	}

	if (!struct_defs) {
		error("orphan %s", oprptr->mnemonic);
		return;
	}
	if (!isspace(*linebeg))
		error("label not allowed on %s", oprptr->mnemonic);
	if (struct_skip)
		struct_skip--;
	if (!--struct_defs) {
		struct_skip = 0;
		st = def_struct;
		def_struct = NULL;
		if (struct_def_name) {
			tlinebeg = linebeg;
			linebeg = struct_def_name;
			struct_enter(st, 1);
			linebeg = tlinebeg;
			as_free(struct_def_name);
			struct_def_name = NULL;
		}
	}
}

/*****************************************************************************/
