/*
 * Copyright (C) 2021 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  globl.h --- global symbol
 *
 */


/* definition */
#define GLOBAL_TYPE_GLOBAL	0
#define GLOBAL_TYPE_XDEF	1
#define GLOBAL_TYPE_XREF	2
#define GLOBAL_TYPE_MASK	3
#define GLOBAL_FLAG_FREE	4

/* structure */
typedef struct gt {
	TREE t;
	char flag;
} GLOBLTABLE;


/* variable */
static GLOBLTABLE *globltbl;		/* global global! */
static GLOBLTABLE *privglobltbl;	/* by source file */


/* prototype */
static void globlop(void);
static GLOBLTABLE *globl_add(char *name, char flag);
static int globl_exist(const char *name);
static void globl_freepriv(void);
#ifdef	CLEANUP
static void globl_cleanup(void);
#endif


