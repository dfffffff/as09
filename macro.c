/*
 * Copyright (C) 2021 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  macro.c --- macro handling
 *
 */


#define MACRO_MAX_PARAM (10+26+26)
#define MACRO_LABEL_CHAR '?'
#define FIND_MACRO(X,Y,Z) (MACROTABLE*)tree_find((X),(TREE**)(Y),(TREE***)(Z))


static MACROTABLE *macrotab, *curmacrodef;
static const char *nomacro[] = {
	"MACR",
	"EXIT",
	"ENDM",
	NULL
};


static
void macro_init(void)
{
	macro_skip = macro_defs = 0;
	curmacrodef = NULL;
	macrostk = NULL;
}


static
void macro_freeparam(void)
{
	int i, num;
	MACROPARAM *param;
	param = macrostk->param;
	if (param) {
		num = param->number;
		for (i=0; i<num; i++)
			as_free(param->param[i]);
		as_free(param->operand);
		as_free(param->param);
		as_free(param);
		macrostk->param = NULL;
	}
}


static
void macro_push(void)
{
	MACROSTACK *ms;
	ms = (MACROSTACK *)as_calloc(1, sizeof(MACROSTACK));
	ms->prev = macrostk;
	ms->cond = cond;
	ms->condstk = condstk;
	macrostk = ms;
	cond = COND_NONE;
	condstk = NULL;
}


static
int macro_pop(int quiet)
{
	MACROSTACK *ms;
	ms = macrostk;
	if (ms) {
		macro_freeparam();
		cond_free(quiet);
		cond = ms->cond;
		condstk = ms->condstk;
		macrostk = ms->prev;
		as_free(ms);
	}
	return !!macrostk;
}


static
void macro_popfile(void)
{
	if (macro_defs)
		error("unterminated macro");
	macro_free();
}


static
void macro_free(void)
{
	while (macro_pop(0));
	curmacrodef = NULL;
	macro_skip = macro_defs = 0;
}


static
void macro_addline(const char *line)
{
	MACROLINE *newline;
	newline = (MACROLINE *)as_calloc(1, sizeof(MACROLINE));
	newline->line = as_strdup(line);
	if (!curmacrodef->lines)
		curmacrodef->lines = newline;
	if (curmacrodef->tail)
		curmacrodef->tail->next = newline;
	curmacrodef->tail = newline;
}


static
void macro_appendchr(char **buffer, int *bufrem, int c)
{
	int rem;
	rem = *bufrem;
	if (rem <= 0)
		fatal("macro line expansion too long",NULL,LTLERROR,2);
	**buffer = c;
	(*buffer)++;
	*bufrem = --rem;
}


static
void macro_appendstr(char **buffer, int *bufrem, const char *str)
{
	while (*str)
		macro_appendchr(buffer, bufrem, *str++);
}


static
void macro_appendnum(char **buffer, int *bufrem, unsigned int num)
{
	char numbuf[MAKEUINT_BUF];
	macro_appendstr(buffer, bufrem, make_uint(numbuf, num));
}


static
void macro_doexpansion(char *buffer, const char *line, int size)
{
	int rem, num, ascarg, len;
	char *ptr, c, esc, chr, com;
	MACROPARAM *param;
	param = macrostk->param;
	ptr = buffer;
	rem = size-1;
	esc = 0;
	ascarg = 0;
	com = iscomment(*line);
	for (; (c=*line); line++) {
		if (c == EOL) {
			macro_appendchr(&ptr, &rem, c);
			break;
		}
		switch (esc) {
		case 0:
			switch (c) {
			case ';':
				com = 1;
				goto appendchr;
			case '\\':
				esc = c;
				break;
			case '\'':
			case '\"':
				if (!com)
					esc = c;
			/* fallthrough */
			default:
appendchr:			macro_appendchr(&ptr, &rem, c);
				break;
			}
			break;
		case '\\':
			switch (c) {
			case '.':
			case '!':
			case '?':
				esc = c;
				break;
			case '*':
				macro_appendstr(&ptr, &rem, param->operand);
				esc = 0;
				break;
			case '#':
				macro_appendnum(&ptr, &rem, param->number);
				esc = 0;
				break;
			default:
				num = alphanumtoint(c);
				if (num < 0)
					error("macro argument number not an alphanumeric");
				else if (num < param->number)
					macro_appendstr(&ptr, &rem, param->param[num]);
				esc = 0;
				break;
			}
			break;
		case '.':
			if (!isalphanumeric(c))
				error("label in macro not an alphanumeric");
			else {
				/* create a globally unique label */
				macro_appendchr(&ptr, &rem, MACRO_LABEL_CHAR);
				macro_appendnum(&ptr, &rem, cursrc->index);
				macro_appendchr(&ptr, &rem, MACRO_LABEL_CHAR);
				macro_appendnum(&ptr, &rem, flineno);
				macro_appendchr(&ptr, &rem, MACRO_LABEL_CHAR);
				macro_appendnum(&ptr, &rem, param->instance);
				macro_appendchr(&ptr, &rem, MACRO_LABEL_CHAR);
				macro_appendstr(&ptr, &rem, macrostk->macro->t.name);
				macro_appendchr(&ptr, &rem, MACRO_LABEL_CHAR);
				macro_appendchr(&ptr, &rem, c);
			}
			esc = 0;
			break;
		case '!':
			if (c == '*')
				len = strlen(param->operand);
			else {
				num = alphanumtoint(c);
				if (num < 0) {
					error("macro argument number not an alphanumeric");
					esc = 0;
					break;
				}
				else if (num >= param->number)
					len = 0;
				else
					len = strlen(param->param[num]);
			}
			macro_appendnum(&ptr, &rem, len);
			esc = 0;
			break;
		case '?':
			num = alphanumtoint(c);
			if (num < 0) {
				error("macro argument number not an alphanumeric");
				esc = 0;
				break;
			}
			ascarg = num;
			esc = 1;
			break;
		case 1:
			num = alphanumtoint(c);
			if (num < 0)
				error("macro character position not an alphanumeric");
			else {
				if (ascarg >= param->number)
					chr = 0;
				else {
					len = strlen(param->param[ascarg]);
					chr = num < len ? param->param[ascarg][num] : 0;
				}
				macro_appendnum(&ptr, &rem, chr);
			}
			esc = 0;
			break;
		case '\'':
		case '\"':
			macro_appendchr(&ptr, &rem, c);
			if (c == esc)
				esc = 0;
			break;
		}
	}
	if (esc)
		error("syntax error in macro expansion");
	*ptr = 0;
}


static
char *macro_getline(char *buffer, int size)
{
	const char *line;
	while (macrostk && !macrostk->line && macro_pop(0));
	if (!macrostk)
		return NULL;
	line = macrostk->line->line;
	macrostk->line = macrostk->line->next;
	macrostk->lineno++;
	macro_doexpansion(buffer, line, size);
	return buffer;
}


static
void macro_addparam(const char *param, int len)
{
	if (!macrostk->param->param)
		macrostk->param->param = (char **)as_calloc(MACRO_MAX_PARAM, sizeof(char*));
	if (macrostk->param->number < MACRO_MAX_PARAM)
		macrostk->param->param[macrostk->param->number++] = as_strndup(param, len);
	else
		error("macro param overflow");
}


/*

 Rules:
  - Maximum limit of 62 arguments.
  - Arguments are separated by comma.
  - Argument wholly enclosed by parentheses,
    the outside parentheses are removed.
  - Single or double quoted argument are passed as is.

 Examples:
   IN                 OUT:   N_ARG  ARG_1       ARG_2       ARG_3       ARG_4        
                   ->        0
   ()              ->        1      [empty]
   (0)             ->        1      0
   (1,2)           ->        1      1,2
   ,               ->        2      [empty]     [empty]
   1,2,3           ->        3      1           2           3
   1,(2,3),(),(4)  ->        4      1           2,3         [empty]     4
   "1 , 2",','     ->        2      "1 , 2"     ','

*/
static
void macro_makeparam(void)
{
	char *base, *begin, *parbeg, *parend, c, esc, sep, unpar;
	int par;
	macrostk->param = (MACROPARAM *)as_calloc(1, sizeof(MACROPARAM));
	macrostk->param->instance = ++cursrc->instance;
	skipspace();
	parbeg = parend = NULL;
	base = begin = lineptr;
	par = 0;
	esc = 0;
	for (; (c=*lineptr); lineptr++) {
		switch (esc) {
		case 0:
			switch (c) {
			case '\'':
			case '\"':
				esc = c;
				break;
			case '(':
				par++;
				if (!parbeg)
					parbeg = lineptr;
				break;
			case ')':
				if (!par)
					error("missing '(' in macro argument");
				else if (!--par && !parend)
					parend = lineptr;
				break;
			default:
				sep = isseparator(c);
				if ((sep && base != lineptr) || (c == ',' && !par)) {
					unpar = parbeg == begin && parend == lineptr-1;
					macro_addparam(begin+unpar, lineptr-begin-unpar-unpar);
					if (sep)
						goto end;
					begin = lineptr+1;
					parbeg = parend = NULL;
				}
				break;
			}
			break;
		case '\'':
		case '\"':
			if (c == EOL)
				break;
			if (c == esc)
				esc = 0;
			break;
		}
	}
end:	macrostk->param->operand = as_strndup(base, *base == EOL ? 0 : (int)(lineptr-base));
	if (esc)
		error("unterminated %c in macro argument", esc);
	else if (par)
		error("missing ')' in macro argument");
}


static
MACROTABLE *macro_check(const char *name)
{
	return FIND_MACRO(name, &macrotab, NULL);
}


static
void macro_enter(MACROTABLE *mt)
{
	macro_push();
	macrostk->macro = mt;
	macrostk->line = mt->lines;
	macro_makeparam();
}


#ifdef	CLEANUP
static
void macro_freelines(MACROLINE *ml)
{
	MACROLINE *next;
	while (ml) {
		next = ml->next;
		as_free(ml->line);
		as_free(ml);
		ml = next;
	}
}

static
void macro_freemacro(MACROTABLE *mt)
{
	if  (mt) {
		macro_freemacro((MACROTABLE*)mt->t.right);
		macro_freemacro((MACROTABLE*)mt->t.left);
		macro_freelines(mt->lines);
		as_free(mt->t.name);
		as_free(mt);
	}
}

static
void macro_cleanup(void)
{
	macro_free();
	macro_freemacro(macrotab);
	macrotab = NULL;
}
#endif

/*****************************************************************************/
/* optab functions */

static
void macroop(void)
{
	int line;
	char *name, *tlineptr;
	const char **ptr;
	MACROTABLE *mt, **mtp, *macro;

	if (macro_defs++) {
		if (macro_skip)
			macro_skip++;
		return;
	}

	if (isspace(*linebeg)) {
		error("%s without name", oprptr->mnemonic);
		goto skip;
	}

	tlineptr = lineptr;
	lineptr = linebeg;
	name = getnewlabel(NULL);
	lineptr = tlineptr;
	if (!name)
		goto skip;
	toupperstr(name);

	macro = macrostk ? macrostk->macro : NULL;
	line = macrostk ? macrostk->lineno : flineno;
	mt = FIND_MACRO(name, &macrotab, &mtp);
	if (mt) {
		if (mt->lineno != line ||
		   (mt->frommacro && mt->frommacro != macro) ||
		   (!mt->frommacro && mt->fromsource != cursrc)) {
			error("multiple defined macro");
			goto free_skip;
		}
		macro_skip++;
		as_free(name);
		goto expand;
	}
	else {
		for (ptr=nomacro; *ptr; ptr++) {
			if (!strcmp(name, *ptr)) {
				error("attempt to define %s as macro", name);
				goto free_skip;
			}
		}
	}

	*mtp = mt = (MACROTABLE *)as_calloc(1, sizeof(MACROTABLE));
	mt->t.name = name;
	mt->lineno = line;
	mt->frommacro = macro;
	mt->fromsource = cursrc;
	curmacrodef = mt;
	nchange++;

expand:
	skipspace();
	mt->expand = *lineptr != EOL ? !!expression() : 1;
	return;

free_skip:
	as_free(name);
skip:
	macro_skip++;
}


static
void endmop(void)
{
	if (!macro_defs) {
		error("orphan %s", oprptr->mnemonic);
		return;
	}
	if (!isspace(*linebeg))
		error("label not allowed on %s", oprptr->mnemonic);
	if (macro_skip)
		macro_skip--;
	if (!--macro_defs) {
		macro_skip = 0;
		curmacrodef = NULL;
	}
}


static
void exitop(void)
{
	if (!macrostk) {
		error("%s out of macro", oprptr->mnemonic);
		return;
	}
	macro_pop(1);
}

/*****************************************************************************/
