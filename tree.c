/*
 * Copyright (C) 2021 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  tree.c --- binary tree
 *
 */

static
TREE *tree_find(const char *name, TREE **tree, TREE ***free)
{
	int i;
	TREE *tp, **tpp;
	if (!*tree)
		tpp = tree;
	else
		for (tp = *tree; ;) {
			if ((i = strcmp(name, tp->name)) == 0) {
				return tp;
			}
			if (i < 0) {
				if (tp->right) {
					tp = tp->right;
				}
				else {
					tpp = &tp->right;
					break;
				}
			}
			else {
				if (tp->left) {
					tp = tp->left;
				}
				else {
					tpp = &tp->left;
					break;
				}
			}
		}
	if (free)
		*free = tpp;
	return NULL;
}

static
TREE *tree_findi(const char *name, TREE **tree, TREE ***free)
{
	int i;
	TREE *tp, **tpp;
	if (!*tree)
		tpp = tree;
	else
		for (tp = *tree; ;) {
			if ((i = as_strcmpi(name, tp->name)) == 0) {
				return tp;
			}
			if (i < 0) {
				if (tp->right) {
					tp = tp->right;
				}
				else {
					tpp = &tp->right;
					break;
				}
			}
			else {
				if (tp->left) {
					tp = tp->left;
				}
				else {
					tpp = &tp->left;
					break;
				}
			}
		}
	if (free)
		*free = tpp;
	return NULL;
}

