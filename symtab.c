/*
 * Copyright (C) 1981, 1987 Masataka Ohta, Hiroshi Tezuka
 * Copyright (C) 2016-2021 David Flamand
 *
 * This file is part of MC6800/6801/6803/6809 cross assembler.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  symtab.c --- symbol table
 *
 */


static
char *getlabel()
{	char *p = lineptr;
	int len = 0;
	if ( (lineptr[0]=='.' && !isalnum(lineptr[1])) || (!isalpha(*lineptr)
#if	WITH_ATLABEL
	     && (!optnodot || *lineptr != '@')
#endif
	   ) )
		error("illegal character in label");
	else
		while ( isalnum(*lineptr) ) {
			lineptr++;
			len++; }
	return as_strndup(p, len);
}

static
char *getnewlabel(unsigned char *lvl)
{	char *lbl;
	lbl = getlabel();
	if ( lvl != NULL ) {
		*lvl = 0;
		while ( *lineptr==':' ) {
			(*lvl)++;
			lineptr++;
		}
	}
	if ( !isseparator(*lineptr) ) {
		*lbl = '\0';
		error("illegal character in label");
	}
	if ( !*lbl ) {
		as_free(lbl);
		lbl = NULL;
	}
	return lbl;
}

static
LBLTABLE *reflabel(int what)
{	LBLTABLE *lp, **lpp;
	char *lbl, *tlbl;
	char *ptr1, buffer1[MAKEUINT_BUF], *ptr2, buffer2[MAKEUINT_BUF], nogbl;
	int duplbl;
	lbl = getlabel();
	nogbl = 0;
	/*** local and non-local label ***/
	if ( !optnodot ) {
		if ( *lbl == '.' ) {
			tlbl = as_strconcat2(parentlabel, lbl);
			as_free(lbl);
			lbl = tlbl;
		}
	}
	else {
		if ( strchr(lbl, '@') ) {
			ptr1 = make_uint(buffer1, cursrc->index);
			ptr2 = make_uint(buffer2, blankline);
			tlbl = as_strconcat5(ptr1, "?", ptr2, "?", lbl);
			as_free(lbl);
			lbl = tlbl;
			nogbl = 1;
		}
	}
	if ( !nogbl ) {
		if ( namespc && !globalize && !globl_exist(lbl) ) {
			tlbl = as_strconcat3(namespc, ".", lbl);
			as_free(lbl);
			lbl = tlbl;
		}
	}
	/*********************************/
	lp = FIND_SYMBOL(lbl, &label, &lpp);
	duplbl = 0;
	if ( lp != NULL ) {
		if ( what == REF_REFERENCED ) {
			if ( !lp->rpass || (!optimize && totpass > lp->rpass) )
				lp = NULL;
		}
		else {
			if ( what == REF_EXPRESSION ) {
				if ( !lp->rpass )
					nchange++;
				lp->rpass = totpass;
			}
			else if ( what == REF_DEFINED ) {
				if ( !optimize && lp->pass && lp->pass < totpass && !lp->inp1 )
					lp = NULL;
			}
			if ( lp != NULL && lp->type == SYM_REFERENCED )
				lp = NULL;
		}
	}
	else {
		if ( what == REF_EXPRESSION ) {
			createlabel(lbl, -1, lpp)->rpass = totpass;
			duplbl = 1;
		}
	}
	if ( reflbl )
		as_free(reflbl);
	reflbl = duplbl ? as_strdup(lbl) : lbl;
	return lp;
}

static
LBLTABLE *deflabel()
{
	char *lbl, *tlbl, add;
	char *ptr1, buffer1[MAKEUINT_BUF], *ptr2, buffer2[MAKEUINT_BUF], nogbl;
	unsigned char lvl;
	LBLTABLE *lp;
	if ( !(lbl=getnewlabel(&lvl)) )
		return NULL;
	add = nogbl = 0;
	/*** local and non-local label ***/
	if ( !optnodot ) {
		if ( *lbl == '.' ) {
			if ( !*parentlabel )
				error("attempt to define a local label before any non-local labels");
			tlbl = as_strconcat2(parentlabel, lbl);
			as_free(lbl);
			lbl = tlbl;
		}
		else if ( !optautopar && !strchr(lbl, '.') ) {
			as_free(parentlabel);
			parentlabel = as_strdup(lbl);
		}
	}
	else {
		if ( strchr(lbl, '@') ) {
			ptr1 = make_uint(buffer1, cursrc->index);
			ptr2 = make_uint(buffer2, blankline);
			tlbl = as_strconcat5(ptr1, "?", ptr2, "?", lbl);
			as_free(lbl);
			lbl = tlbl;
			nogbl = 1;
		}
	}
	if ( !nogbl ) {
		if ( !FIND_GLOBL(lbl, &globltbl, NULL)) {
			if ( !globalize ) {
				if ( !FIND_GLOBL(lbl, &privglobltbl, NULL) ) {
					if ( namespc ) {
						tlbl = as_strconcat3(namespc, ".", lbl);
						as_free(lbl);
						lbl = tlbl;
					}
				}
			}
			else
				add = 1;
		}
	}
	/*********************************/
	lp = createlabel(lbl, lvl, NULL);
	if ( add )
		globl_add(lp->t.name, GLOBAL_TYPE_GLOBAL);
	return lp;
}

static
LBLTABLE *createlabel(char *name, int level, LBLTABLE **lpp)
{
	LBLTABLE *lp;
	int lcvalue;
	if ( !lpp && (lp=FIND_SYMBOL(name, &label, &lpp)) ) {
		if ( lp->type == SYM_REFERENCED ) {
			as_free(name);
			name = lp->t.name;
		}
		else {
			if ( lp->type != SYM_SET ) {
				if ( lp->fline != flineno || lp->sline != slineno || lp->source != cursrc )
					error("multiple defined label");
				else {
					if ( lp->type == SYM_LABEL && !(optdatsep && lp->man == 1)
#if	WITH_OS9
						&& lp->man != 2
#endif
					   ) {
						lcvalue = lcvalid ? llc : 0;
						if ( lp->valid!=lcvalid || lp->value!=lcvalue )
							nchange++;
						lp->valid = lcvalid;
						lp->value = lcvalue;
						lp->pass = totpass;
						lp->man = 0;
						lp->inp1 = inp1;
					}
					lblptr = lp;
				}
			}
			else {
				setlabel = 1;
				lblptr = lp;
			}
			as_free(name);
			return lp;
		}
	}
	else
		lp = *lpp = (LBLTABLE*)as_calloc(1,sizeof(LBLTABLE));
	if ( level >= 0 ) {
		labels++;
		lp->valid = lcvalid;
		lp->value = llc;
		lp->fline = flineno;
		lp->sline = slineno;
		lp->lline = llineno;
		lp->source = cursrc;
		lp->level = level > 255 ? 255 : level;
		lp->type = SYM_LABEL;
		lp->pass = totpass;
		lp->inp1 = inp1;
		lblptr = lp;
	}
	lp->t.name = name;
	nchange++;
	return lp;
}

static
void defsymbol(const char *str)
{	LBLTABLE *lp;
	char *newstr, *eq, eol[2];
	if ( str && !strchr(str,EOL) )
	{	eol[0]=EOL; eol[1]=0;
		newstr = as_strconcat2(str,eol);
		eq = strchr(newstr, '=');
		if ( eq )
			*eq = ' ';
		lineptr = newstr;
		lp = deflabel();
		if ( !lp )
			goto errorfree;
		if ( eq )
		{	lineptr = eq + 1;
			noundef = 1;
			lp->value = expression();
			lp->valid = valid;
		}
		else
		{	lp->value = 1;
			lp->valid = 1;
		}
		lp->type = SYM_SET;
		skipspace();
		if ( *lineptr != EOL )
			goto errorfree;
		as_free(newstr);
		return;
errorfree:
		as_free(newstr);
	}
	error("syntax error");
}

#ifdef	CLEANUP
static
void freenode(LBLTABLE *node)
{
	if ( node != NULL ) {
		if (node->t.right)
			freenode((LBLTABLE*)node->t.right);
		if (node->t.left)
			freenode((LBLTABLE*)node->t.left);
		as_free(node->t.name);
		as_free(node);
	}
}
#endif
